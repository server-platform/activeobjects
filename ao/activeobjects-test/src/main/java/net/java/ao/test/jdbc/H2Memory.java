package net.java.ao.test.jdbc;

/**
 * Configuration settings for the H2 instance backed by memory.
 */
public class H2Memory extends H2 {
    private static final String DEFAULT_URL = makeDefaultUrl();

    private static String makeDefaultUrl() {
        StringBuilder url = new StringBuilder("jdbc:h2:mem:ao-test");
        // In this mode some compatibility features for applications written for H2 1.X are enabled. This mode doesn't
        // provide full compatibility with H2 1.X.
        // For eg, Empty IN, TOP clause, MINUS, IDENTITY, etc
        if (h2VersionCompareTo(2, 1, 200) >= 0) {
            url.append(";MODE=LEGACY");
        }
        appendDriverSettings(url);
        return url.toString();
    }

    public H2Memory() {
        super(DEFAULT_URL);
    }

    public H2Memory(String url, String username, String password, String schema) {
        super(url, username, password, schema);
    }

    @Override
    protected String getDefaultUrl() {
        return DEFAULT_URL;
    }
}
