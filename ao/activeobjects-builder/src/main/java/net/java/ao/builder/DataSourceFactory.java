package net.java.ao.builder;

import java.sql.Driver;

import net.java.ao.DisposableDataSource;

/**
 * Allow creation of a {@link net.java.ao.DisposableDataSource disposable data source} from given
 * connection properties.
 */
public interface DataSourceFactory {
    DisposableDataSource getDataSource(
            Class<? extends Driver> driverClass, String url, String username, String password);
}
