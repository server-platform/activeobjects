package net.java.ao.it;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import net.java.ao.DatabaseProvider;
import net.java.ao.Entity;
import net.java.ao.schema.Default;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.StringLength;
import net.java.ao.schema.Unique;
import net.java.ao.test.ActiveObjectsIntegrationTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.emptyIterable;

public class TestNoExtraMigrations extends ActiveObjectsIntegrationTest {

    @Test
    public void shouldNotMigrateIntegerColumns() throws SQLException {
        checkExtraMigrations(IntegerTable.class);
    }

    @Test
    public void shouldNotMigrateLongColumns() throws SQLException {
        checkExtraMigrations(LongTable.class);
    }

    @Test
    public void shouldNotMigrateDoubleColumns() throws SQLException {
        checkExtraMigrations(DoubleTable.class);
    }

    @Test
    public void shouldNotMigrateBooleanColumns() throws SQLException {
        checkExtraMigrations(BooleanTable.class);
    }

    @Test
    public void shouldNotMigrateTextColumns() throws SQLException {
        checkExtraMigrations(TextTable.class);
    }

    private void checkExtraMigrations(final Class entityClass) throws SQLException {
        entityManager.migrate(entityClass);

        final SqlMemorizingListener listener = new SqlMemorizingListener();
        entityManager.getProvider().addSqlListener(listener);
        entityManager.migrate(entityClass);

        assertThat(listener.sqls, emptyIterable());
    }

    public static class SqlMemorizingListener implements DatabaseProvider.SqlListener {
        private final List<String> sqls = new ArrayList<>();

        @Override
        public void onSql(String sql) {
            sqls.add(sql);
        }
    }

    public interface IntegerTable extends Entity {
        @NotNull
        Integer getIntegerNonNull();

        void setIntegerNonNull(Integer i);

        Integer getIntegerNullable();

        void setIntegerNullable(Integer i);

        @Default("10")
        Integer getIntegerWithDefaultValue();

        void setIntegerWithDefaultValue(Integer i);

        @Default("10")
        @NotNull
        Integer getIntegerNotNullDefault();

        void setIntegerNotNullDefault(Integer i);

        @Default("10")
        @Unique
        Integer getIntegerUniqueDefault();

        void setIntegerUniqueDefault(Integer i);
    }

    public interface LongTable extends Entity {
        @NotNull
        Long getLongNotNull();

        void setLongNotNull(Long l);

        Long getLongNullable();

        void setLongNullable(Long l);

        @Default("10")
        Long getLongWithDefaultValue();

        void setLongWithDefaultValue(Long l);

        @Default("10")
        @NotNull
        Long getLongNotNullDefault();

        void setLongNotNullDefault(Long l);

        @Default("10")
        @Unique
        Long getLongUniqueDefault();

        void setLongUniqueDefault(Long l);
    }

    public interface DoubleTable extends Entity {
        @NotNull
        Double getDoubleNotNull();

        void setDoubleNotNull(Double d);

        Double getDoubleNullable();

        void setDoubleNullable(Double d);

        @Default("12.3")
        Double getDoubleWithDefaultValue();

        void setDoubleWithDefaultValue(Double d);

        @Default("12.3")
        @NotNull
        Double getDoubleNotNullDefault();

        void setDoubleNotNullDefault(Double d);

        @Default("12.3")
        @Unique
        Double getDoubleUniqueDefault();

        void setDoubleUniqueDefault(Double d);
    }

    public interface BooleanTable extends Entity {
        @NotNull
        Boolean getBooleanNotNull();

        void setBooleanNotNull(Boolean b);

        Boolean getBooleanNullable();

        void setBooleanNullable(Boolean b);

        @Default("true")
        Boolean getBooleanDefaultValue();

        void setBooleanDefaultValue(Boolean b);

        @Default("true")
        @NotNull
        Boolean getBooleanNotNullDefault();

        void setBooleanNotNullDefault(Boolean b);

        @Default("true")
        @Unique
        Boolean getBooleanUniqueDefault();

        void setBooleanUniqueDefault(Boolean b);
    }

    public interface TextTable extends Entity {
        @NotNull
        @StringLength(StringLength.UNLIMITED)
        String getUnlimitedText();

        void setUnlimitedText(String text);

        @StringLength(StringLength.UNLIMITED)
        String getUnlimitedNullableText();

        void setUnlimitedNullableText(String text);

        @NotNull
        @StringLength(20)
        @Default("default text")
        String getSmallDefaultText();

        void setSmallDefaultText(String text);

        @StringLength(20)
        @Default("default text")
        String getSmallDefaultNullableText();

        void setSmallDefaultNullableText(String text);

        @NotNull
        @StringLength(20)
        String getSmallText();

        void setSmallText(String text);

        @StringLength(20)
        String getSmallNullableText();

        void setSmallNullableText(String text);
    }
}
