package net.java.ao.it;

import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import net.java.ao.Query;
import net.java.ao.it.model.Score;
import net.java.ao.it.model.ScoreWithCount;
import net.java.ao.test.ActiveObjectsIntegrationTest;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;

/**
 * Integration test for groupBy query.
 *
 * This tests present the idea behind groupBy in ao.
 * You need to make sure that returned values fit the object's field names.
 */
public class GroupByTest extends ActiveObjectsIntegrationTest {

    @Before
    public void createData() throws Exception {
        //noinspection unchecked
        entityManager.migrate(Score.class);

        saveScore("Dota", "Levi", 10);
        saveScore("CS", "Levi", 86);
        saveScore("Apex", "Levi", 360);

        saveScore("Dota", "Matthew", 100);
        saveScore("CS", "Matthew", 200);
    }

    /*
     * Please remember that not all aggregate functions are same in all databases.
     * Please refrain from using this functionality if not needed or not sure.
     */
    @Test
    public void testSimpleMaxQueryReturnsCorrectResultsWithStream() throws SQLException {
        final Query query =
                Query.select("PLAYER, MAX(SCORE) as SCORE").group("PLAYER").order("PLAYER");
        final List<Score> scores = new LinkedList<>();

        entityManager.stream(Score.class, query, scores::add);

        assertThat(scores.size(), is(2));
        assertThat(scores.get(0).getPlayer(), is("Levi"));
        assertThat(scores.get(0).getScore(), is(360));
        assertThat(scores.get(1).getPlayer(), is("Matthew"));
        assertThat(scores.get(1).getScore(), is(200));
    }

    @Test
    public void testEffectiveDistinctQueryReturnsCorrectResultsWithStream() throws SQLException {
        // Set up
        final Query query = Query.select("PLAYER").group("PLAYER").order("PLAYER DESC");
        final List<Score> scores = new LinkedList<>();

        // Invoke
        entityManager.stream(Score.class, query, scores::add);

        // Check
        assertThat(scores.size(), is(2));
        assertThat(scores.get(0).getPlayer(), is("Matthew"));
        assertThat(scores.get(1).getPlayer(), is("Levi"));
    }

    /*
     * This query works because the ScoreWithCount class (which as you can see from the @Table annotation maps to the
     * SCORE table) adds a dummy "COUNT" column that AO populates with the result of calling COUNT(GAME) (thanks to the
     * alias on that result, which is also called "COUNT"). This is a bit of a hack.
     */
    @Test
    public void testCountQueryReturnsCorrectResultsWithStream() throws SQLException {
        final Query query =
                Query.select("PLAYER, COUNT(GAME) as COUNT").group("PLAYER").order("PLAYER");
        final List<ScoreWithCount> scores = new LinkedList<>();

        entityManager.stream(ScoreWithCount.class, query, scores::add);

        assertThat(scores.size(), is(2));
        assertThat(scores.get(0).getPlayer(), is("Levi"));
        assertThat(scores.get(0).getCount(), is(3));
        assertThat(scores.get(1).getPlayer(), is("Matthew"));
        assertThat(scores.get(1).getCount(), is(2));
    }

    /**
     * One day find may work with group by.
     * However today is not the day.
     * <p>
     * It is because find returns editable entities,
     * where stream returns read only ones.
     * <p>
     * It is much harder for find to fit to all cases with group by then.
     */
    @Test
    @Ignore
    public void testEffectiveDistinctQueryReturnsCorrectResultsWithFind() throws Exception {
        final Query query = Query.select("PLAYER").group("PLAYER").order("PLAYER");

        final List<Score> scores =
                Stream.of(entityManager.find(Score.class, query)).collect(toList());

        assertThat(scores.size(), is(2));
        final List<String> players = scores.stream().map(Score::getPlayer).collect(toList());
        assertThat(players, contains("Levi", "Matthew"));
    }

    private void saveScore(final String game, final String player, final int score) throws SQLException {
        final Score scoreObj = entityManager.create(Score.class);

        scoreObj.setPlayer(player);
        scoreObj.setGame(game);
        scoreObj.setScore(score);

        scoreObj.save();
    }
}
