package net.java.ao;

import java.util.regex.Matcher;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class QueryAliasPatternTest {

    @Test
    public void shouldCorrectlyRecognizeLowercaseAliasedColumn() {
        final Matcher matcher = Query.ALIAS_PATTERN.matcher("column as alias");

        final boolean result = matcher.find();

        assertThat(result, is(true));
        assertThat(matcher.group(1), is("column"));
        assertThat(matcher.group(2), is("alias"));
    }

    @Test
    public void shouldCorrectlyRecognizeUppercaseAliasedColumn() {
        final Matcher matcher = Query.ALIAS_PATTERN.matcher("COLUMN AS ALIAS");

        final boolean result = matcher.find();

        assertThat(result, is(true));
        assertThat(matcher.group(1), is("COLUMN"));
        assertThat(matcher.group(2), is("ALIAS"));
    }

    @Test
    public void shouldCorrectlyRecognizeAliasedColumnWithAggregateFunction() {
        final Matcher matcher = Query.ALIAS_PATTERN.matcher("count(column) as alias");

        final boolean result = matcher.find();

        assertThat(result, is(true));
        assertThat(matcher.group(1), is("count(column)"));
        assertThat(matcher.group(2), is("alias"));
    }

    @Test
    public void shouldCorrectlyRecognizeAliasedColumnWithQuotesAndWhitespace() {
        final Matcher matcher = Query.ALIAS_PATTERN.matcher("count(\"column with whitespace\") as alias");

        final boolean result = matcher.find();

        assertThat(result, is(true));
        assertThat(matcher.group(1), is("count(\"column with whitespace\")"));
        assertThat(matcher.group(2), is("alias"));
    }

    @Test
    public void shouldOnlyTreatLastAsAsAliasSeparator() {
        final Matcher matcher = Query.ALIAS_PATTERN.matcher("count(\"column as whitespace\") as alias");

        final boolean result = matcher.find();

        assertThat(result, is(true));
        assertThat(matcher.group(1), is("count(\"column as whitespace\")"));
        assertThat(matcher.group(2), is("alias"));
    }

    @Test
    public void shouldCorrectlyRecognizeAliasWithoutAs() {
        final Matcher matcher = Query.ALIAS_PATTERN.matcher("column alias");

        final boolean result = matcher.find();

        assertThat(result, is(true));
        assertThat(matcher.group(1), is("column"));
        assertThat(matcher.group(2), is("alias"));
    }

    @Test
    public void shouldCorrectlyRecognizeMultiWordAliasWithoutAs() {
        final Matcher matcher = Query.ALIAS_PATTERN.matcher("column alias 2");

        final boolean result = matcher.find();

        assertThat(result, is(true));
        assertThat(matcher.group(1), is("column alias"));
        assertThat(matcher.group(2), is("2"));
    }

    @Test
    public void shouldCorrectlyRecognizeMultiWordAliasWithoutAsWithUnorthodoxWhitespace() {
        final Matcher matcher = Query.ALIAS_PATTERN.matcher("column  alias	2");

        final boolean result = matcher.find();

        assertThat(result, is(true));
        assertThat(matcher.group(1), is("column  alias"));
        assertThat(matcher.group(2), is("2"));
    }
}
