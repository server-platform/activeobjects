package net.java.ao.schema;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import com.google.common.collect.ImmutableList;

import net.java.ao.Entity;
import net.java.ao.RawEntity;
import net.java.ao.it.DatabaseProcessor;
import net.java.ao.schema.ddl.DDLField;
import net.java.ao.schema.ddl.DDLIndex;
import net.java.ao.schema.ddl.DDLTable;
import net.java.ao.test.ActiveObjectsIntegrationTest;
import net.java.ao.test.jdbc.Data;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Daniel Spiewak
 */
@Data(DatabaseProcessor.class)
public final class SchemaGeneratorUniqueTest extends ActiveObjectsIntegrationTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @SuppressWarnings("null")
    @Test
    public void parsesUniqueIndexes() {
        final TableNameConverter tableNameConverter =
                entityManager.getNameConverters().getTableNameConverter();
        DDLTable parsedTable = SchemaGenerator.parseDDL(
                entityManager.getProvider(), entityManager.getNameConverters(), SomeEntity.class)[0];
        final String tableName = tableNameConverter.getName(SomeEntity.class);

        List<DDLIndex> expectedIndexes = ImmutableList.<DDLIndex>builder()
                .add(
                        DDLIndex.builder()
                                .field(field("getFirst", Integer.class, SomeEntity.class))
                                .table(tableName)
                                .indexName(indexName(tableName, "ix1"))
                                .build(),
                        DDLIndex.builder()
                                .fields(
                                        field("getFirst", Integer.class, SomeEntity.class),
                                        field("getSecond", Integer.class, SomeEntity.class))
                                .table(tableName)
                                .indexName(indexName(tableName, "ix2"))
                                .unique()
                                .build())
                .build();

        assertEquals(expectedIndexes.size(), parsedTable.getIndexes().length);
        assertThat(Arrays.asList(parsedTable.getIndexes()), containsInAnyOrder(expectedIndexes.toArray()));

        DDLField[] fields = parsedTable.getFields();
        DDLField first = Arrays.stream(fields)
                .filter(f -> Objects.equals(f.getName(), getFieldName(SomeEntity.class, "getFirst")))
                .findFirst()
                .get();
        assertFalse(first.isUnique());

        DDLField second = Arrays.stream(fields)
                .filter(f -> Objects.equals(f.getName(), getFieldName(SomeEntity.class, "getSecond")))
                .findFirst()
                .get();
        assertFalse(second.isUnique());

        DDLField third = Arrays.stream(fields)
                .filter(f -> Objects.equals(f.getName(), getFieldName(SomeEntity.class, "getThird")))
                .findFirst()
                .get();
        assertFalse(third.isUnique());

        DDLField fourth = Arrays.stream(fields)
                .filter(f -> Objects.equals(f.getName(), getFieldName(SomeEntity.class, "getFourth")))
                .findFirst()
                .get();
        assertTrue(fourth.isUnique());
    }

    @SuppressWarnings("null")
    @Test
    public void uniqueFieldIsEquivalentToUniqueIndexWithOneField() {
        final TableNameConverter tableNameConverter =
                entityManager.getNameConverters().getTableNameConverter();
        List<DDLField[]> fields = new ArrayList<>();

        for (Class<? extends RawEntity<?>> clazz : ImmutableList.of(SomeEntity.class, SomeOtherEntity.class)) {
            DDLTable parsedTable =
                    SchemaGenerator.parseDDL(entityManager.getProvider(), entityManager.getNameConverters(), clazz)[0];
            final String tableName = tableNameConverter.getName(clazz);

            List<DDLIndex> expectedIndexes = ImmutableList.<DDLIndex>builder()
                    .add(
                            DDLIndex.builder()
                                    .field(field("getFirst", Integer.class, SomeEntity.class))
                                    .table(tableName)
                                    .indexName(indexName(tableName, "ix1"))
                                    .build(),
                            DDLIndex.builder()
                                    .fields(
                                            field("getFirst", Integer.class, SomeEntity.class),
                                            field("getSecond", Integer.class, SomeEntity.class))
                                    .table(tableName)
                                    .indexName(indexName(tableName, "ix2"))
                                    .unique()
                                    .build())
                    .build();

            assertEquals(expectedIndexes.size(), parsedTable.getIndexes().length);
            assertThat(Arrays.asList(parsedTable.getIndexes()), containsInAnyOrder(expectedIndexes.toArray()));

            fields.add(parsedTable.getFields());
        }

        DDLField[] someFields = fields.get(0);
        DDLField[] someOtherFields = fields.get(1);
        for (int i = 0; i < someFields.length; i++) {
            assertEquals(someFields[i].getName(), someOtherFields[i].getName());
            assertEquals(someFields[i].isUnique(), someOtherFields[i].isUnique());
        }
    }

    @SuppressWarnings("null")
    @Test
    public void whenClassHasUniqueIndexOnUniqueFieldNoDuplicateIndexesAreCreated() {
        final TableNameConverter tableNameConverter =
                entityManager.getNameConverters().getTableNameConverter();
        List<DDLField[]> fields = new ArrayList<>();

        for (Class<? extends RawEntity<?>> clazz :
                ImmutableList.of(SomeEntity.class, AnEntityWithUniqueFieldAndIndex.class)) {
            DDLTable parsedTable =
                    SchemaGenerator.parseDDL(entityManager.getProvider(), entityManager.getNameConverters(), clazz)[0];
            final String tableName = tableNameConverter.getName(clazz);

            List<DDLIndex> expectedIndexes = ImmutableList.<DDLIndex>builder()
                    .add(
                            DDLIndex.builder()
                                    .field(field("getFirst", Integer.class, SomeEntity.class))
                                    .table(tableName)
                                    .indexName(indexName(tableName, "ix1"))
                                    .build(),
                            DDLIndex.builder()
                                    .fields(
                                            field("getFirst", Integer.class, SomeEntity.class),
                                            field("getSecond", Integer.class, SomeEntity.class))
                                    .table(tableName)
                                    .indexName(indexName(tableName, "ix2"))
                                    .unique()
                                    .build())
                    .build();

            assertEquals(expectedIndexes.size(), parsedTable.getIndexes().length);
            assertThat(Arrays.asList(parsedTable.getIndexes()), containsInAnyOrder(expectedIndexes.toArray()));

            fields.add(parsedTable.getFields());
        }

        DDLField[] someFields = fields.get(0);
        DDLField[] someOtherFields = fields.get(1);
        for (int i = 0; i < someFields.length; i++) {
            assertEquals(someFields[i].getName(), someOtherFields[i].getName());
            assertEquals(someFields[i].isUnique(), someOtherFields[i].isUnique());
        }
    }

    @Indexes({
        @Index(
                name = "ix1",
                methodNames = {"getFirst"}),
        @Index(
                name = "ix2",
                methodNames = {"getFirst", "getSecond"},
                unique = true),
        @Index(
                name = "ix3",
                methodNames = {"getFourth"},
                unique = true),
    })
    public interface SomeEntity extends Entity {
        int getFirst();

        void setFirst(int value);

        int getSecond();

        void setSecond(int value);

        int getThird();

        void setThird(int value);

        int getFourth();

        void setFourth(int value);
    }

    @Indexes({
        @Index(
                name = "ix1",
                methodNames = {"getFirst"}),
        @Index(
                name = "ix2",
                methodNames = {"getFirst", "getSecond"},
                unique = true),
    })
    public interface SomeOtherEntity extends Entity {
        int getFirst();

        void setFirst(int value);

        int getSecond();

        void setSecond(int value);

        int getThird();

        void setThird(int value);

        @Unique
        int getFourth();

        void setFourth(int value);
    }

    @Indexes({
        @Index(
                name = "ix1",
                methodNames = {"getFirst"}),
        @Index(
                name = "ix2",
                methodNames = {"getFirst", "getSecond"},
                unique = true),
        @Index(
                name = "ix3",
                methodNames = {"getFourth"},
                unique = true),
    })
    public interface AnEntityWithUniqueFieldAndIndex extends Entity {
        int getFirst();

        void setFirst(int value);

        int getSecond();

        void setSecond(int value);

        int getThird();

        void setThird(int value);

        @Unique
        int getFourth();

        void setFourth(int value);
    }
}
