package net.java.ao;

import java.util.regex.Matcher;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class QueryAggregateFunctionPatternTest {

    @Test
    public void shouldMatchLowercaseAggregateFunctionWithoutWhitespace() {
        final Matcher matcher = Query.AGGREGATE_FUNCTION_PATTERN.matcher("count2(column1)");

        final boolean result = matcher.find();

        assertThat(result, is(true));
        assertThat(matcher.group(1), is("count2"));
        assertThat(matcher.group(2), is("column1"));
    }

    @Test
    public void shouldMatchUppercaseAggregateFunctionWithoutWhitespace() {
        final Matcher matcher = Query.AGGREGATE_FUNCTION_PATTERN.matcher("COUNT2(COLUMN1)");

        final boolean result = matcher.find();

        assertThat(result, is(true));
        assertThat(matcher.group(1), is("COUNT2"));
        assertThat(matcher.group(2), is("COLUMN1"));
    }

    @Test
    public void shouldNotMatchAggregateFunctionWithWhitespace() {
        final Matcher matcher = Query.AGGREGATE_FUNCTION_PATTERN.matcher("count2       (column1)");

        final boolean result = matcher.find();

        assertThat(result, is(false));
    }

    @Test
    public void shouldNotMatchColumnName() {
        final Matcher matcher = Query.AGGREGATE_FUNCTION_PATTERN.matcher("column");

        final boolean result = matcher.find();

        assertThat(result, is(false));
    }

    @Test
    public void shouldNotMatchUnterminatedParenthesis() {
        final Matcher matcher = Query.AGGREGATE_FUNCTION_PATTERN.matcher("column(");

        final boolean result = matcher.find();

        assertThat(result, is(false));
    }
}
