package net.java.ao.it;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import net.java.ao.Accessor;
import net.java.ao.EntityManager;
import net.java.ao.Generator;
import net.java.ao.RawEntity;
import net.java.ao.ValueGenerator;
import net.java.ao.schema.AutoIncrement;
import net.java.ao.schema.NotNull;
import net.java.ao.schema.PrimaryKey;
import net.java.ao.test.ActiveObjectsIntegrationTest;
import net.java.ao.test.jdbc.Data;
import net.java.ao.test.jdbc.DatabaseUpdater;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;

@Data(TestBatchInsertion.BatchInsertionDatabaseUpdater.class)
public class TestBatchInsertion extends ActiveObjectsIntegrationTest {
    private static final String NAME_COLUMN = "ENTITY_NAME";
    private static final String TEXT_COLUMN = "ENTITY_TEXT";
    private static final String TIMESTAMP_COLUMN = "ENTITY_TIMESTAMP";

    @Test
    public void testBatchInsertion() throws Exception {
        entityManager.create(
                BatchInsertionEntity.class,
                ImmutableList.of(
                        ImmutableMap.of(NAME_COLUMN, "foo", TEXT_COLUMN, "foo is good"),
                        ImmutableMap.of(NAME_COLUMN, "bar", TEXT_COLUMN, "bar is ugly")));

        List<BatchInsertionEntity> entities = new ArrayList<>();
        entityManager.stream(BatchInsertionEntity.class, entities::add);

        assertThat(entities.size(), is(2));
        assertThat(
                entities,
                containsInAnyOrder(
                        hasValues("foo", "foo is good", 2718281828L), hasValues("bar", "bar is ugly", 2718281828L)));
    }

    @Test
    public void testBatchInsertionWithNullValue() throws Exception {
        entityManager.create(
                BatchInsertionEntity.class,
                ImmutableList.of(
                        ImmutableMap.of(NAME_COLUMN, "foo", TEXT_COLUMN, "foo is good"),
                        ImmutableMap.of(NAME_COLUMN, "bar")));

        List<BatchInsertionEntity> entities = new ArrayList<>();
        entityManager.stream(BatchInsertionEntity.class, entities::add);

        assertThat(entities.size(), is(2));
        assertThat(
                entities,
                containsInAnyOrder(hasValues("foo", "foo is good", 2718281828L), hasValues("bar", null, 2718281828L)));
    }

    @Test
    public void testBatchInsertionWithFirstNullValue() throws Exception {
        entityManager.create(
                BatchInsertionEntity.class,
                ImmutableList.of(
                        ImmutableMap.of(NAME_COLUMN, "foo"),
                        ImmutableMap.of(NAME_COLUMN, "bar", TEXT_COLUMN, "bar is ugly")));

        final List<BatchInsertionEntity> entities = new ArrayList<>();
        entityManager.stream(BatchInsertionEntity.class, entities::add);

        assertThat(entities.size(), is(2));
        assertThat(
                entities,
                containsInAnyOrder(hasValues("foo", null, 2718281828L), hasValues("bar", "bar is ugly", 2718281828L)));
    }

    public static class BatchInsertionDatabaseUpdater implements DatabaseUpdater {
        @Override
        public void update(EntityManager entityManager) throws Exception {
            entityManager.migrate(BatchInsertionEntity.class);
        }
    }

    private static Matcher<BatchInsertionEntity> hasValues(final String name, final String desc, final long timestamp) {
        return new TypeSafeMatcher<BatchInsertionEntity>() {
            @Override
            public boolean matchesSafely(BatchInsertionEntity entity) {
                return Objects.equals(name, entity.getName())
                        && Objects.equals(desc, entity.getDescription())
                        && entity.getTimestamp() == timestamp;
            }

            @Override
            public void describeTo(Description description) {
                description
                        .appendText("entity should be {name=")
                        .appendValue(name)
                        .appendText(", description=")
                        .appendValue(desc)
                        .appendText("}");
            }

            @Override
            public void describeMismatchSafely(BatchInsertionEntity entity, Description description) {
                description
                        .appendText("was {name=")
                        .appendValue(entity.getName())
                        .appendText(" ,description=")
                        .appendValue(entity.getDescription())
                        .appendText("}");
            }
        };
    }

    public static class BatchInsertionEntityTimestampGenerator implements ValueGenerator<Long> {
        public Long generateValue(EntityManager manager) {
            return 2718281828L;
        }
    }

    public interface BatchInsertionEntity extends RawEntity<Long> {
        @AutoIncrement
        @NotNull
        @PrimaryKey("ID")
        Long getId();

        @Accessor(TIMESTAMP_COLUMN)
        @Generator(BatchInsertionEntityTimestampGenerator.class)
        @NotNull
        Long getTimestamp();

        @Accessor(NAME_COLUMN)
        String getName();

        @Accessor(TEXT_COLUMN)
        String getDescription();
    }
}
