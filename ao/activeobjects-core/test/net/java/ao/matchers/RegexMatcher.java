package net.java.ao.matchers;

import java.util.regex.Pattern;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

public class RegexMatcher extends TypeSafeMatcher<String> {

    private final Pattern pattern;

    public RegexMatcher(String regex) {
        this.pattern = Pattern.compile(regex);
    }

    @Override
    protected boolean matchesSafely(String item) {
        return pattern.matcher(item).matches();
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("a string matching the regex '" + pattern + "'");
    }
}
