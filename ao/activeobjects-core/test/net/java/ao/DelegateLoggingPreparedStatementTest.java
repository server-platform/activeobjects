package net.java.ao;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import com.google.common.collect.ImmutableMap;

import net.java.ao.sql.LoggingInterceptor;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DelegateLoggingPreparedStatementTest {
    @Mock
    private LoggingInterceptor mockedLoggingInterceptor;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private PreparedStatement mockedPreparedStatement;

    private PreparedStatement preparedStatement;

    @Before
    public void setUp() {
        preparedStatement = new DelegateLoggingPreparedStatement(
                mockedPreparedStatement, mockedLoggingInterceptor, "some sql where id = ?");
    }

    @Test
    public void shouldLogSuccessfulPreparedStatementExecution() throws SQLException {
        final Map<Integer, String> params = ImmutableMap.of(
                1, "16",
                2, "A");

        preparedStatement.setInt(1, 16);
        preparedStatement.setString(2, "A");

        preparedStatement.execute();

        verify(mockedLoggingInterceptor).beforeExecution();
        verify(mockedLoggingInterceptor).afterSuccessfulExecution("some sql where id = ?", params);
        verify(mockedLoggingInterceptor, never()).onException(any(), any(), any());
    }

    @Test
    public void shouldLogPreparedStatementException() throws SQLException {
        SQLException exception = new SQLException();
        when(mockedPreparedStatement.execute(anyString())).thenThrow(exception);

        preparedStatement.setString(1, "16");

        try {
            preparedStatement.execute("some sql where id = ?");
        } catch (final SQLException ignored) {
        }

        verify(mockedLoggingInterceptor).beforeExecution();
        verify(mockedLoggingInterceptor, never()).afterSuccessfulExecution(any(), any());
        verify(mockedLoggingInterceptor).onException("some sql where id = ?", ImmutableMap.of(1, "16"), exception);
    }
}
