package net.java.ao;

/**
 * This class exists only so that we don't have to expose the {@link CachingSqlProcessor} internals and still test them from the outside of their package.
 */
public final class CachingSqlProcessorHackcessor {
    private CachingSqlProcessorHackcessor() {}

    public static int getProcessedWhereClausesSize(final CachingSqlProcessor cachingSqlProcessor) {
        return (int) cachingSqlProcessor.processedWhereClauses.size();
    }

    public static CachingSqlProcessor newCachingSqlProcessor() {
        return new CachingSqlProcessor();
    }
}
