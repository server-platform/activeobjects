package net.java.ao;

import org.junit.runner.RunWith;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.junit.ArchUnitRunner;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

@RunWith(ArchUnitRunner.class)
@AnalyzeClasses(packages = "net.java.ao..")
public class SeparationOfCodeTest {
    @ArchTest
    public static final ArchRule aoLibrary_shouldBeSeparateFromTheAtlassianPlatform = noClasses()
            .that()
            .resideInAPackage("net.java.ao..")
            // This is the exclusion list of existing code, do not add to this, only remove
            .and()
            .doNotHaveFullyQualifiedName("net.java.ao.EntityManager")
            .and()
            .doNotHaveFullyQualifiedName("net.java.ao.db.H2DatabaseProvider")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("com.atlassian..", "io.atlassian..");
}
