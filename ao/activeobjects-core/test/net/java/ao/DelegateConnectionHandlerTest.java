package net.java.ao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DelegateConnectionHandlerTest {

    @Mock
    private Connection mockedConnection;

    private DelegateConnection delegateConnection;

    @Before
    public void setUp() {
        delegateConnection = DelegateConnectionHandler.newInstance(mockedConnection, true);
    }

    @Test
    public void shouldWrapStatementInDelegateLoggingStatement() throws SQLException {
        final Statement statement = delegateConnection.createStatement();

        assertThat(statement, CoreMatchers.instanceOf(DelegateLoggingStatement.class));
    }

    @Test
    public void shouldWrapPreparedStatementInDelegateLoggingPreparedStatement() throws SQLException {
        final Statement statement = delegateConnection.prepareStatement("some sql");

        assertThat(statement, CoreMatchers.instanceOf(DelegateLoggingPreparedStatement.class));
    }
}
