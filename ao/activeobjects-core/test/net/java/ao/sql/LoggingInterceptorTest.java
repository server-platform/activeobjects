package net.java.ao.sql;

import java.time.Clock;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.Logger;
import com.google.common.collect.ImmutableMap;

import static org.mockito.ArgumentMatchers.matches;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class LoggingInterceptorTest {

    @Mock
    private Logger logger;

    @Mock
    private Clock clock;

    private LoggingInterceptor interceptor;

    private Map<Integer, String> params;

    @Before
    public void setUp() {
        params = ImmutableMap.of(
                1, "50",
                2, "A");

        when(clock.millis()).thenReturn(4L).thenReturn(7L);
        when(logger.isInfoEnabled()).thenReturn(true);
        interceptor = new LoggingInterceptor(logger, new CallStackProvider(), clock);
    }

    @Test
    public void shouldLogQueryWithParamsAndExecutionTime() {
        interceptor.beforeExecution();
        interceptor.afterSuccessfulExecution("some sql", params);

        verify(logger).debug("3 ms \"some sql\" {1=50, 2=A}");
    }

    @Test
    public void shouldLogCallStack() {
        interceptor = new LoggingInterceptor(logger, new CallStackProvider(), clock, true);
        when(logger.isDebugEnabled()).thenReturn(true);

        interceptor.beforeExecution();
        interceptor.afterSuccessfulExecution("some sql", params);

        verify(logger).debug(matches("3 ms \"some sql\" \\{1=50, 2=A\\}\\ncall stack\\n\\t...\\n[\\s\\S]*\\t..."));
    }

    @Test
    public void shouldLogExceptionExecution() {
        Exception exception = new Exception();

        interceptor.beforeExecution();
        interceptor.onException("some sql", params, exception);

        verify(logger).error("3 ms \"some sql\" {1=50, 2=A}", exception);
    }
}
