package net.java.ao.schema;

import java.lang.reflect.Method;

import net.java.ao.Mutator;

public final class MutatorFieldNameResolver extends AbstractFieldNameResolver {
    public MutatorFieldNameResolver() {
        super(false);
    }

    @Override
    public boolean accept(Method method) {
        return method.isAnnotationPresent(Mutator.class);
    }

    @Override
    public String resolve(Method method) {
        return method.getAnnotation(Mutator.class).value();
    }
}
