package net.java.ao;

import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * A prepared statement that caches parameter metadata.
 */
public class ParameterMetadataCachingPreparedStatement extends ForwardingPreparedStatement {
    private ParameterMetaData parameterMetaData;

    ParameterMetadataCachingPreparedStatement(PreparedStatement statement) {
        super(statement);
    }

    @Override
    public ParameterMetaData getParameterMetaData() throws SQLException {
        if (parameterMetaData == null) {
            parameterMetaData = statement.getParameterMetaData();
        }
        return parameterMetaData;
    }
}
