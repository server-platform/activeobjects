/*
 * Copyright 2007 Daniel Spiewak
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *	    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.java.ao;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.Clock;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.java.ao.sql.CallStackProvider;
import net.java.ao.sql.LoggingInterceptor;

final class DelegateConnectionHandler implements InvocationHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(DelegateConnectionHandler.class);
    private static final CallStackProvider CALL_STACK_PROVIDER = new CallStackProvider();
    public static final String CREATE_STATEMENT_METHOD = "createStatement";
    public static final String PREPARE_STATEMENT_METHOD = "prepareStatement";

    private final Connection delegate;
    private boolean closeable;
    private boolean extraLogging;

    private DelegateConnectionHandler(Connection delegate) {
        this.delegate = Objects.requireNonNull(delegate);
        this.closeable = true;
        this.extraLogging = false;
    }

    private DelegateConnectionHandler(Connection delegate, boolean extraLogging) {
        this(delegate);

        this.extraLogging = extraLogging;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (isSetCloseableMethod(method)) {
            setCloseable((Boolean) args[0]);
            return Void.TYPE;
        } else if (isIsCloseableMethod(method)) {
            return isCloseable();
        } else if (isCloseMethod(method)) {
            close();
            return Void.TYPE;
        } else if (isIsClosedMethod(method)) {
            return isClosed();
        }

        if (isExtraLoggingEnabled()) {
            if (method.getName().equals(CREATE_STATEMENT_METHOD)) {
                return new DelegateLoggingStatement(
                        (Statement) delegate(method, args),
                        new LoggingInterceptor(
                                LOGGER, CALL_STACK_PROVIDER, Clock.systemUTC(), isCallStackLoggingEnabled()));
            } else if (method.getName().equals(PREPARE_STATEMENT_METHOD)) {
                return new DelegateLoggingPreparedStatement(
                        (PreparedStatement) delegate(method, args),
                        new LoggingInterceptor(
                                LOGGER, CALL_STACK_PROVIDER, Clock.systemUTC(), isCallStackLoggingEnabled()),
                        (String) args[0]);
            }
        }

        return delegate(method, args);
    }

    private boolean isCallStackLoggingEnabled() {
        return Boolean.getBoolean("net.java.ao.sql.logging.callstack");
    }

    private boolean isExtraLoggingEnabled() {
        return extraLogging;
    }

    private void setCloseable(boolean closeable) {
        this.closeable = closeable;
    }

    private boolean isCloseable() {
        return closeable;
    }

    private void close() throws SQLException {
        if (isCloseable()) {
            delegate.close();
        }
    }

    private boolean isClosed() throws SQLException {
        return delegate.isClosed();
    }

    private Object delegate(Method method, Object[] args) throws Throwable {
        final Method m = delegate.getClass().getMethod(method.getName(), method.getParameterTypes());
        m.setAccessible(true);
        try {
            return m.invoke(delegate, args);
        } catch (InvocationTargetException e) {
            throw e.getCause();
        }
    }

    public static DelegateConnection newInstance(Connection c) {
        return newInstance(c, false);
    }

    public static DelegateConnection newInstance(Connection c, boolean extraLogging) {
        return (DelegateConnection) Proxy.newProxyInstance(
                DelegateConnectionHandler.class.getClassLoader(),
                new Class[] {DelegateConnection.class},
                new DelegateConnectionHandler(c, extraLogging));
    }

    private static boolean isSetCloseableMethod(Method method) {
        return method.getName().equals("setCloseable")
                && method.getParameterTypes().length == 1
                && method.getParameterTypes()[0].equals(boolean.class);
    }

    private static boolean isIsCloseableMethod(Method method) {
        return method.getName().equals("isCloseable") && method.getParameterTypes().length == 0;
    }

    private static boolean isCloseMethod(Method method) {
        return method.getName().equals("close") && method.getParameterTypes().length == 0;
    }

    private static boolean isIsClosedMethod(Method method) {
        return method.getName().equals("isClosed") && method.getParameterTypes().length == 0;
    }
}
