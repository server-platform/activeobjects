package net.java.ao.sql;

import com.google.common.base.Throwables;

public class CallStackProvider {

    public String getCallStack() {
        final String stackTrace = Throwables.getStackTraceAsString(new Throwable());

        return stackTrace;
    }
}
