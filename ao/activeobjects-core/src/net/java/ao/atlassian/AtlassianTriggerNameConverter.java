package net.java.ao.atlassian;

import java.util.Objects;

import net.java.ao.schema.DefaultTriggerNameConverter;
import net.java.ao.schema.TriggerNameConverter;

import static net.java.ao.Common.shorten;

public final class AtlassianTriggerNameConverter implements TriggerNameConverter {
    private final TriggerNameConverter delegate;

    public AtlassianTriggerNameConverter() {
        this(new DefaultTriggerNameConverter());
    }

    public AtlassianTriggerNameConverter(TriggerNameConverter delegate) {
        this.delegate = Objects.requireNonNull(delegate, "delegate can't be null");
    }

    @Override
    public String autoIncrementName(String tableName, String columnName) {
        return shorten(delegate.autoIncrementName(tableName, columnName), ConverterUtils.MAX_LENGTH);
    }
}
