package net.java.ao.types;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Set;

import net.java.ao.EntityManager;

import static java.util.Arrays.stream;
import static java.util.Collections.unmodifiableSet;
import static java.util.stream.Collectors.toSet;

abstract class AbstractLogicalType<T> implements LogicalType<T> {
    private final String name;
    private final Set<Class<?>> types;
    private final int defaultJdbcWriteType;
    private final Set<Integer> jdbcReadTypes;

    protected AbstractLogicalType(String name, Class<?>[] types, int defaultJdbcWriteType, Integer[] jdbcReadTypes) {
        this.name = name;
        this.types = unmodifiableSet(stream(types).collect(toSet()));
        this.defaultJdbcWriteType = defaultJdbcWriteType;
        this.jdbcReadTypes = unmodifiableSet(stream(jdbcReadTypes).collect(toSet()));
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Set<Class<?>> getAllTypes() {
        return types;
    }

    @Override
    public Set<Integer> getAllJdbcReadTypes() {
        return jdbcReadTypes;
    }

    @Override
    public int getDefaultJdbcWriteType() {
        return defaultJdbcWriteType;
    }

    @Override
    public boolean isAllowedAsPrimaryKey() {
        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object validate(Object value) throws IllegalArgumentException {
        if ((value != null) && !isSupportedType(value.getClass())) {
            throw new IllegalArgumentException(
                    "Value of class " + value.getClass() + " is not valid for column type " + getName());
        }
        return validateInternal((T) value);
    }

    private boolean isSupportedType(Class<?> clazz) {
        for (Class<?> type : types) {
            if (type.isAssignableFrom(clazz)) {
                return true;
            }
        }
        return false;
    }

    protected T validateInternal(T value) throws IllegalArgumentException {
        return value;
    }

    @Override
    public void putToDatabase(EntityManager manager, PreparedStatement stmt, int index, T value, int jdbcType)
            throws SQLException {
        stmt.setObject(index, value, jdbcType);
    }

    @Override
    public T pullFromDatabase(EntityManager manager, ResultSet res, Class<T> type, int columnIndex)
            throws SQLException {
        return pullFromDatabase(manager, res, type, res.getMetaData().getColumnName(columnIndex));
    }

    @Override
    public boolean shouldCache(Class<?> type) {
        return shouldStore(type);
    }

    @Override
    public boolean shouldStore(final Class<?> type) {
        return true;
    }

    @Override
    public T parse(String input) throws IllegalArgumentException {
        throw new IllegalArgumentException("Cannot parse a string into type " + getName());
    }

    @Override
    public T parseDefault(String input) throws IllegalArgumentException {
        return parse(input);
    }

    @Override
    public boolean valueEquals(Object value1, Object value2) {
        return Objects.equals(value1, value2);
    }

    @Override
    public String valueToString(T value) {
        return String.valueOf(value);
    }

    protected static <T> T preserveNull(ResultSet res, T value) throws SQLException {
        return res.wasNull() ? null : value;
    }

    @Override
    public boolean equals(Object other) {
        return (getClass() == other.getClass());
    }

    @Override
    public String toString() {
        return getName();
    }
}
