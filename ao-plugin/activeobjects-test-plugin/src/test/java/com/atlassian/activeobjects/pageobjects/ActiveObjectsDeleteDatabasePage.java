package com.atlassian.activeobjects.pageobjects;

import javax.inject.Inject;

import com.atlassian.pageobjects.Page;
import com.atlassian.webdriver.WebDriverContext;

public class ActiveObjectsDeleteDatabasePage implements Page {
    @Inject
    protected WebDriverContext driver;

    @Override
    public String getUrl() {
        return "/plugins/servlet/ao-test?delete=true";
    }
}
