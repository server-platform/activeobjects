package it.com.atlassian.activeobjects;

import org.junit.Test;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import static it.com.atlassian.activeobjects.MetricsTest.adminJsonRequest;
import static it.com.atlassian.activeobjects.MetricsTest.getNoisyNeighbourUrl;
import static it.com.atlassian.activeobjects.MetricsTest.triggerOperationSuccessfully;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;

public class EntityManagerInTransactionMetricsTest {

    private static final double LENGTH_OF_IN_TRANSACTION_TASK = 1_000d;
    private static final double MINIMUM_COUNT = 1d;

    @Test
    public void whenAnAoIsExecutedInTransaction_theJmxBeansIncludeTheExecuteInTransactionTaskBean() {
        String operation = "AO_EXECUTE_IN_TRANSACTION";
        triggerOperationSuccessfully(operation);

        String jmxOutput = "category00=db,category01=ao,name=executeInTransaction,"
                + "tag.invokerPluginKey=com.atlassian.diagnostics.noisy-neighbour-plugin,tag.taskName="
                + "com.atlassian.diagnostics.noisyneighbour.operations.database.entity.AOEntityManagerInTransactionOperation";

        Response taskAttributes = getTaskAttribute(jmxOutput);

        assertThat("Task is timed", getMean(taskAttributes), greaterThan(LENGTH_OF_IN_TRANSACTION_TASK));
        assertThat("Task is counted", getCount(taskAttributes), greaterThanOrEqualTo(MINIMUM_COUNT));
    }

    private Response getTaskAttribute(String taskBean) {
        return adminJsonRequest()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .when()
                .get(getNoisyNeighbourUrl("/jmx/search?minCount=1&name=" + taskBean));
    }

    private double getMean(Response taskAttributes) {
        return Double.parseDouble(taskAttributes.jsonPath().get("Mean"));
    }

    private double getCount(Response taskAttributes) {
        return Double.parseDouble(taskAttributes.jsonPath().get("Count"));
    }
}
