package it.com.atlassian.activeobjects;

import javax.ws.rs.core.Response;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

public final class MetricsTest {

    private MetricsTest() {}

    static void triggerOperationSuccessfully(String operation) {
        adminJsonRequest()
                .given()
                .body(operation)
                .contentType(ContentType.JSON)
                .when()
                .post(getNoisyNeighbourUrl("/admin/block"))
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .statusCode(Response.Status.OK.getStatusCode());
    }

    static String getNoisyNeighbourUrl(String path) {
        String baseurl = System.getProperty("baseurl", "http://localhost:5990/refapp");

        return baseurl + "/rest/noisyneighbour/latest" + path;
    }

    static String getAdminUser() {
        return "admin";
    }

    static String getAdminPassword() {
        return "admin";
    }

    static RequestSpecification adminJsonRequest() {
        return RestAssured.given()
                .auth()
                .preemptive()
                .basic(getAdminUser(), getAdminPassword())
                .contentType(ContentType.JSON);
    }
}
