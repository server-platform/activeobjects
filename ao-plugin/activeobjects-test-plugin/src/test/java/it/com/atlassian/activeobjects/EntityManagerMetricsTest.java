package it.com.atlassian.activeobjects;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import io.restassured.http.ContentType;

import static it.com.atlassian.activeobjects.MetricsTest.adminJsonRequest;
import static it.com.atlassian.activeobjects.MetricsTest.getNoisyNeighbourUrl;
import static it.com.atlassian.activeobjects.MetricsTest.triggerOperationSuccessfully;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class EntityManagerMetricsTest {
    private static final String NOISY_NEIGHBOUR_PLUGIN_KEY = "com.atlassian.diagnostics.noisy-neighbour-plugin";
    private static final String NOISY_NEIGHBOUR_ENTITY_TYPE =
            "com.atlassian.diagnostics.noisyneighbour.operations.database.entity.NoisyNeighbourEntity";
    private static final String ENTITY_MANAGER_METRIC = "category00=db,category01=ao,category02=entityManager,name=";

    private List<String> mBeanNames;

    @Before
    public void setUp() {
        String operationTask = "AO_ENTITY_MANAGER";
        triggerOperationSuccessfully(operationTask);

        mBeanNames = Arrays.asList(adminJsonRequest()
                .then()
                .log()
                .ifValidationFails()
                .statusCode(200)
                .contentType(ContentType.JSON)
                .when()
                .get(getNoisyNeighbourUrl("/jmx"))
                .as(String[].class));
    }

    @Test
    public void whenEntityManagerCreateIsCalled_theJmxBeansIncludeTheCorrectBean() {
        Optional<String> entityManagerCreateBean = getBean(ENTITY_MANAGER_METRIC + "create");

        assertThat(
                "Entity Manager Create metric bean has been emitted to JMX",
                entityManagerCreateBean.isPresent(),
                is(true));
    }

    @Test
    public void whenEntityManagerFindIsCalled_theJmxBeansIncludeTheCorrectBean() {
        Optional<String> entityManagerFindBean = getBean(ENTITY_MANAGER_METRIC + "find");

        assertThat(
                "Entity Manager Find metric bean has been emitted to JMX", entityManagerFindBean.isPresent(), is(true));
    }

    @Test
    public void whenEntityManagerGetIsCalled_theJmxBeansIncludeTheCorrectBean() {
        Optional<String> entityManagerGetBean = getBean(ENTITY_MANAGER_METRIC + "get");

        assertThat(
                "Entity Manager Get metric bean has been emitted to JMX", entityManagerGetBean.isPresent(), is(true));
    }

    @Test
    public void whenEntityManagerDeleteIsCalled_theJmxBeansIncludeTheCorrectBean() {
        Optional<String> entityManagerDeleteBean = getBean(ENTITY_MANAGER_METRIC + "delete");

        assertThat(
                "Entity Manager Delete metric bean has been emitted to JMX",
                entityManagerDeleteBean.isPresent(),
                is(true));
    }

    @Test
    public void whenEntityManagerCountIsCalled_theJmxBeansIncludeTheCorrectBean() {
        Optional<String> entityManagerCountBean = getBean(ENTITY_MANAGER_METRIC + "count");

        assertThat(
                "Entity Manager Count metric bean has been emitted to JMX",
                entityManagerCountBean.isPresent(),
                is(true));
    }

    @Test
    public void whenEntityManagerDeleteWithSQLIsCalled_theJmxBeansIncludeTheCorrectBean() {
        Optional<String> entityManagerDeleteWithSQLBean = getBean(ENTITY_MANAGER_METRIC + "deleteWithSQL");

        assertThat(
                "Entity Manager DeleteWithSQL metric bean has been emitted to JMX",
                entityManagerDeleteWithSQLBean.isPresent(),
                is(true));
    }

    @Test
    public void whenEntityManagerStreamIsCalled_theJmxBeansIncludeTheCorrectBean() {
        Optional<String> entityManagerStreamBean = getBean(ENTITY_MANAGER_METRIC + "stream");

        assertThat(
                "Entity Manager Stream metric bean has been emitted to JMX",
                entityManagerStreamBean.isPresent(),
                is(true));
    }

    private Optional<String> getBean(String metricName) {
        return mBeanNames.stream()
                // Match on the entity manager metric
                .filter(n -> n.contains(metricName))
                .filter(n -> n.contains(NOISY_NEIGHBOUR_PLUGIN_KEY))
                .filter(n -> n.contains(NOISY_NEIGHBOUR_ENTITY_TYPE))
                .findFirst();
    }
}
