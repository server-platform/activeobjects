package com.atlassian.dbexporter.node.stax;

import java.util.List;

import static java.util.Arrays.asList;

public final class XmlFactoryException extends RuntimeException {
    private final List<Throwable> throwables;

    public XmlFactoryException(String s, Throwable... throwables) {
        super(s, throwables[throwables.length - 1]);
        this.throwables = asList(throwables);
    }

    public List<Throwable> getThrowables() {
        return throwables;
    }
}
