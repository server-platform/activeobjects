package com.atlassian.dbexporter;

import java.util.List;

import com.atlassian.activeobjects.spi.ImportExportException;
import com.atlassian.dbexporter.exporter.ExportConfiguration;
import com.atlassian.dbexporter.exporter.Exporter;
import com.atlassian.dbexporter.node.NodeCreator;
import com.atlassian.dbexporter.node.NodeStreamWriter;
import com.atlassian.dbexporter.progress.ProgressMonitor;

import static java.util.Arrays.asList;
import static java.util.Objects.requireNonNull;

import static com.atlassian.dbexporter.node.NodeBackup.RootNode;

/**
 * Creates an export of a database. What exactly the export 'looks' like depends heavily on the exporters passed-in.
 * <p>
 * A {@link ProgressMonitor} can be supplied to be notified of the progress as the export is being made.
 *
 * @author Erik van Zijst
 * @author Samuel Le Berrigaud
 */
public final class DbExporter {

    private final List<Exporter> exporters;

    /**
     * Constructor.
     *
     * @param exporters the exporters
     * @throws IllegalArgumentException if {@code exporters} is {@code null} or empty
     */
    public DbExporter(final Exporter... exporters) {
        this(asList(requireNonNull(exporters)));
    }

    /**
     * Constructor.
     *
     * @param exporters the exporters
     * @throws IllegalArgumentException if {@code exporters} is {@code null} or empty
     */
    public DbExporter(final List<Exporter> exporters) {
        if (requireNonNull(exporters).isEmpty()) {
            throw new IllegalArgumentException("DbExporter must be created with at least one Exporter!");
        }
        this.exporters = exporters;
    }

    /**
     * Imports the XML document read from the stream
     *
     * @param streamWriter  the stream to write the XML to
     * @param configuration the export configuration
     * @throws ImportExportException or one of its sub-types if an unexpected exception happens during the import.
     */
    public void exportData(NodeStreamWriter streamWriter, ExportConfiguration configuration) {
        final ProgressMonitor monitor = configuration.getProgressMonitor();
        monitor.begin();

        final NodeCreator node = RootNode.add(streamWriter);
        final Context context = new Context();
        for (Exporter exporter : exporters) {
            exporter.export(node, configuration, context);
        }

        monitor.end();
    }
}
