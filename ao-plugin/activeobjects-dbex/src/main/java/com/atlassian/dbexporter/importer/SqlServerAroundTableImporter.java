package com.atlassian.dbexporter.importer;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.NoSuchElementException;

import com.atlassian.dbexporter.Column;
import com.atlassian.dbexporter.Context;
import com.atlassian.dbexporter.DatabaseInformations.Database.Type;
import com.atlassian.dbexporter.ImportExportErrorService;
import com.atlassian.dbexporter.Table;
import com.atlassian.dbexporter.importer.DataImporter.AroundTableImporter;

import static java.util.Objects.requireNonNull;

import static com.atlassian.dbexporter.DatabaseInformations.database;
import static com.atlassian.dbexporter.jdbc.JdbcUtils.closeQuietly;
import static com.atlassian.dbexporter.jdbc.JdbcUtils.quote;

public final class SqlServerAroundTableImporter implements AroundTableImporter {
    private final ImportExportErrorService errorService;
    private final String schema;

    public SqlServerAroundTableImporter(ImportExportErrorService errorService, String schema) {
        this.errorService = requireNonNull(errorService);
        this.schema = schema;
    }

    @Override
    public void before(ImportConfiguration configuration, Context context, String table, Connection connection) {
        setIdentityInsert(configuration, context, connection, table, "ON");
    }

    @Override
    public void after(ImportConfiguration configuration, Context context, String table, Connection connection) {
        setIdentityInsert(configuration, context, connection, table, "OFF");
    }

    private void setIdentityInsert(
            ImportConfiguration configuration, Context context, Connection connection, String table, String onOff) {
        if (isSqlServer(configuration) && isAutoIncrementTable(context, table)) {
            setIdentityInsert(connection, table, onOff);
        }
    }

    private boolean isAutoIncrementTable(Context context, final String tableName) {
        return hasAnyAutoIncrementColumn(findTable(context, tableName));
    }

    private boolean hasAnyAutoIncrementColumn(final Table table) {
        return table.getColumns().stream().anyMatch(Column::isAutoIncrement);
    }

    private Table findTable(final Context context, final String tableName) {
        requireNonNull(context.getAll(Table.class));
        return context.getAll(Table.class).stream()
                .filter(table -> table.getName().equals(tableName))
                .findAny()
                .orElseThrow(NoSuchElementException::new);
    }

    private void setIdentityInsert(Connection connection, String table, String onOff) {
        Statement s = null;
        try {
            s = connection.createStatement();
            s.execute(setIdentityInsertSql(quote(errorService, table, connection, table), onOff));
        } catch (SQLException e) {
            throw errorService.newImportExportSqlException(table, "", e);
        } finally {
            closeQuietly(s);
        }
    }

    private String setIdentityInsertSql(String table, String onOff) {
        return String.format("SET IDENTITY_INSERT %s %s", schema != null ? schema + "." + table : table, onOff);
    }

    private boolean isSqlServer(ImportConfiguration configuration) {
        return Type.MSSQL.equals(
                database(configuration.getDatabaseInformation()).getType());
    }
}
