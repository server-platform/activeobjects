package com.atlassian.dbexporter;

import static java.util.Objects.requireNonNull;

public class ForeignKey {

    private final String fromTable;
    private final String fromField;
    private final String toTable;
    private final String toField;

    public ForeignKey(final String fromTable, final String fromField, final String toTable, final String toField) {
        this.fromTable = requireNonNull(fromTable);
        this.fromField = requireNonNull(fromField);
        this.toTable = requireNonNull(toTable);
        this.toField = requireNonNull(toField);
    }

    public String getFromTable() {
        return fromTable;
    }

    public String getFromField() {
        return fromField;
    }

    public String getToTable() {
        return toTable;
    }

    public String getToField() {
        return toField;
    }
}
