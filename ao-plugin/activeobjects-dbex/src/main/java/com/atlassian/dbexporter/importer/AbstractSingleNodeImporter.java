package com.atlassian.dbexporter.importer;

import java.util.List;

import com.atlassian.dbexporter.ImportExportErrorService;
import com.atlassian.dbexporter.node.NodeParser;

import static java.util.Objects.requireNonNull;

public abstract class AbstractSingleNodeImporter extends AbstractImporter {

    protected AbstractSingleNodeImporter(final ImportExportErrorService errorService) {
        super(errorService);
    }

    protected AbstractSingleNodeImporter(
            final ImportExportErrorService errorService, final List<AroundImporter> arounds) {
        super(errorService, arounds);
    }

    public final boolean supports(final NodeParser node) {
        return requireNonNull(node).getName().equals(getNodeName());
    }

    protected abstract String getNodeName();
}
