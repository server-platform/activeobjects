package com.atlassian.dbexporter;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static java.util.Collections.unmodifiableCollection;
import static java.util.Collections.unmodifiableList;
import static java.util.Objects.requireNonNull;

public final class Table {

    private final String name;
    private final List<Column> columns;
    private final Collection<ForeignKey> foreignKeys;

    public Table(final String name, final List<Column> columns, final Collection<ForeignKey> foreignKeys) {
        this.name = requireNonNull(name);
        this.columns = new LinkedList<>(requireNonNull(columns));
        this.foreignKeys = new LinkedList<>(requireNonNull(foreignKeys));
    }

    public String getName() {
        return name;
    }

    public List<Column> getColumns() {
        return unmodifiableList(columns);
    }

    public Collection<ForeignKey> getForeignKeys() {
        return unmodifiableCollection(foreignKeys);
    }
}
