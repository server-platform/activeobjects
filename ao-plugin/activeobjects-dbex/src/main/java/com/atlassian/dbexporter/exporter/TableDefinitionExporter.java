package com.atlassian.dbexporter.exporter;

import java.util.Collection;

import com.atlassian.dbexporter.Column;
import com.atlassian.dbexporter.Context;
import com.atlassian.dbexporter.DatabaseInformation;
import com.atlassian.dbexporter.ForeignKey;
import com.atlassian.dbexporter.Table;
import com.atlassian.dbexporter.node.NodeCreator;
import com.atlassian.dbexporter.progress.ProgressMonitor;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

import static com.atlassian.dbexporter.node.NodeBackup.ColumnDefinitionNode;
import static com.atlassian.dbexporter.node.NodeBackup.ForeignKeyDefinitionNode;
import static com.atlassian.dbexporter.node.NodeBackup.TableDefinitionNode;
import static com.atlassian.dbexporter.progress.ProgressMonitor.Task;

public final class TableDefinitionExporter implements Exporter {

    private final TableReader tableReader;

    public TableDefinitionExporter(final TableReader tableReader) {
        this.tableReader = requireNonNull(tableReader);
    }

    @Override
    public void export(final NodeCreator node, final ExportConfiguration configuration, final Context context) {
        final ProgressMonitor monitor = configuration.getProgressMonitor();
        monitor.begin(Task.TABLE_DEFINITION);

        int tableCount = 0;
        final Iterable<Table> tables =
                tableReader.read(getDatabaseInformation(context), configuration.getEntityNameProcessor());
        for (Table table : tables) {
            export(node, table);
            tableCount++;
        }
        monitor.end(Task.TABLE_DEFINITION);
        monitor.totalNumberOfTables(tableCount);

        final Collection<Table> tablesToInsert =
                stream(tables.spliterator(), false).collect(toList());
        context.putAll(tablesToInsert);
    }

    private DatabaseInformation getDatabaseInformation(final Context context) {
        return requireNonNull(context.get(DatabaseInformation.class));
    }

    private void export(final NodeCreator node, final Table table) {
        TableDefinitionNode.add(node);
        TableDefinitionNode.setName(node, table.getName());

        for (Column column : table.getColumns()) {
            export(node, column);
        }

        for (ForeignKey foreignKey : table.getForeignKeys()) {
            export(node, foreignKey);
        }

        node.closeEntity();
    }

    private void export(final NodeCreator node, final Column column) {
        ColumnDefinitionNode.add(node);
        ColumnDefinitionNode.setName(node, column.getName());
        ColumnDefinitionNode.setPrimaryKey(node, column.isPrimaryKey());
        ColumnDefinitionNode.setAutoIncrement(node, column.isAutoIncrement());
        ColumnDefinitionNode.setSqlType(node, column.getSqlType());
        ColumnDefinitionNode.setPrecision(node, column.getPrecision());
        ColumnDefinitionNode.setScale(node, column.getScale());
        node.closeEntity();
    }

    private void export(final NodeCreator node, final ForeignKey foreignKey) {
        ForeignKeyDefinitionNode.add(node);
        ForeignKeyDefinitionNode.setFromTable(node, foreignKey.getFromTable());
        ForeignKeyDefinitionNode.setFromColumn(node, foreignKey.getFromField());
        ForeignKeyDefinitionNode.setToTable(node, foreignKey.getToTable());
        ForeignKeyDefinitionNode.setToColumn(node, foreignKey.getToField());
        node.closeEntity();
    }
}
