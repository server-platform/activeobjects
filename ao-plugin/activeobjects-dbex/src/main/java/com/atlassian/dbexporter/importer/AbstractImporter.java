package com.atlassian.dbexporter.importer;

import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.dbexporter.Context;
import com.atlassian.dbexporter.ImportExportErrorService;
import com.atlassian.dbexporter.node.NodeParser;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;

public abstract class AbstractImporter implements Importer {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected final ImportExportErrorService errorService;
    private final List<AroundImporter> arounds;

    protected AbstractImporter(final ImportExportErrorService errorService) {
        this(errorService, emptyList());
    }

    protected AbstractImporter(final ImportExportErrorService errorService, final List<AroundImporter> arounds) {
        this.errorService = requireNonNull(errorService);
        this.arounds = requireNonNull(arounds);
    }

    @Override
    public final void importNode(
            final NodeParser node, final ImportConfiguration configuration, final Context context) {
        requireNonNull(node);
        if (node.isClosed()) {
            throw new IllegalArgumentException("Node must not be closed to be imported! " + node);
        }
        if (!supports(node)) {
            throw new IllegalArgumentException("Importer called on unsupported node! " + node);
        }

        requireNonNull(context);

        logger.debug("Importing node {}", node);

        for (AroundImporter around : arounds) {
            around.before(node, configuration, context);
        }

        doImportNode(node, configuration, context);

        for (ListIterator<AroundImporter> iterator = arounds.listIterator(arounds.size()); iterator.hasPrevious(); ) {
            iterator.previous().after(node, configuration, context);
        }
    }

    protected abstract void doImportNode(NodeParser node, ImportConfiguration configuration, Context context);
}
