package com.atlassian.dbexporter.importer;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.dbexporter.Context;
import com.atlassian.dbexporter.DatabaseInformation;
import com.atlassian.dbexporter.ImportExportErrorService;
import com.atlassian.dbexporter.node.NodeParser;
import com.atlassian.dbexporter.progress.ProgressMonitor;

import static java.util.Objects.requireNonNull;

import static com.atlassian.dbexporter.importer.ImporterUtils.checkEndNode;
import static com.atlassian.dbexporter.importer.ImporterUtils.checkStartNode;
import static com.atlassian.dbexporter.node.NodeBackup.DatabaseInformationNode;
import static com.atlassian.dbexporter.node.NodeBackup.DatabaseInformationNode.getMetaKey;
import static com.atlassian.dbexporter.node.NodeBackup.DatabaseInformationNode.getMetaValue;
import static com.atlassian.dbexporter.progress.ProgressMonitor.Task;

public final class DatabaseInformationImporter extends AbstractSingleNodeImporter {

    private final DatabaseInformationChecker infoChecker;

    public DatabaseInformationImporter(final ImportExportErrorService errorService) {
        this(errorService, NoOpDatabaseInformationChecker.INSTANCE);
    }

    public DatabaseInformationImporter(
            final ImportExportErrorService errorService, final DatabaseInformationChecker infoChecker) {
        super(errorService);
        this.infoChecker = requireNonNull(infoChecker);
    }

    @Override
    protected void doImportNode(final NodeParser node, final ImportConfiguration configuration, final Context context) {
        final ProgressMonitor monitor = configuration.getProgressMonitor();
        monitor.begin(Task.DATABASE_INFORMATION);

        final DatabaseInformation info = doImportDatabaseInformation(node);
        infoChecker.check(info);
        context.put(info);

        monitor.end(Task.DATABASE_INFORMATION);
    }

    private DatabaseInformation doImportDatabaseInformation(final NodeParser node) {
        final Map<String, String> meta = new HashMap<>();
        checkStartNode(node, DatabaseInformationNode.NAME);
        doImportMetas(node.getNextNode(), meta);
        checkEndNode(node, DatabaseInformationNode.NAME);
        node.getNextNode(); // done with the database information
        return new DatabaseInformation(meta);
    }

    private void doImportMetas(final NodeParser node, final Map<String, String> meta) {
        while (node.getName().equals(DatabaseInformationNode.META)) {
            doImportMeta(node, meta);
        }
    }

    private void doImportMeta(final NodeParser node, final Map<String, String> meta) {
        checkStartNode(node, DatabaseInformationNode.META);
        meta.put(getMetaKey(node), getMetaValue(node));
        checkEndNode(node.getNextNode(), DatabaseInformationNode.META);
        node.getNextNode(); // go to next node
    }

    @Override
    protected String getNodeName() {
        return DatabaseInformationNode.NAME;
    }

    private static final class NoOpDatabaseInformationChecker implements DatabaseInformationChecker {

        private static final DatabaseInformationChecker INSTANCE = new NoOpDatabaseInformationChecker();

        private NoOpDatabaseInformationChecker() {}

        public void check(DatabaseInformation information) {
            // no-op
        }
    }
}
