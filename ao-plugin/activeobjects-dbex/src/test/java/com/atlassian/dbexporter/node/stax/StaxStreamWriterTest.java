package com.atlassian.dbexporter.node.stax;

import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import javax.xml.bind.DatatypeConverter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.dbexporter.ImportExportErrorService;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class StaxStreamWriterTest {
    @Mock
    private ImportExportErrorService errorService;

    private Writer output;

    private StaxStreamWriter staxStreamWriter;

    @Before
    public void setUp() {
        output = new StringWriter();
        staxStreamWriter = new StaxStreamWriter(errorService, output, StandardCharsets.UTF_8, "");
    }

    @Test
    public void nodeCreatorShouldEncodeBinaryAsBase64() {
        final byte[] bytes = new byte[] {0, 1, 100, 2};

        staxStreamWriter.addRootNode("root").setContentAsBinary(bytes);
        staxStreamWriter.close();

        assertThat(output.toString(), containsString(DatatypeConverter.printBase64Binary(bytes)));
    }
}
