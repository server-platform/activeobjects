# Changelog
All notable changes to the Active Objects plugin will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [3.2.5] - unreleased

* [BSP-1220]: Make AO plugins transformerless:
  * `activeobjects-test-plugin`
  * `activeobjects-confluence-spi` 

## [3.2.3] - 2019-02-20

* [QUICK-1294]: Fixed a bug in batch insert when first row contains null value

## [3.2.2] - 2019-12-18

* [BSP-907]: Upgrade mysql driver to 8.0.18 to support new build plans 

## [3.2.0] - 2019-12-18

* [QUICK-780]: Add support for batch insert

## [3.1.9] - 2020-01-21

### Changed

* [KRAK-206]: Migrate platform CI builds that use postgres/mysql to docker.

### Fixed

* [BSP-722] DoS vulnerability through Woodstox dependency through AO plugin
* [BSP-787] SCM info is wrong in AO lib POM.
* [BSP-564] AO plugin's Oracle 12c ojdbc5 and ojdbc6 jobs are failing.
* [BSP-413] Re-enable SQL Server tests for AO plugin.

## [3.1.7] - 2019-07-25

### Fixed

* [CONFSRVDEV-11431]: Error log during run setupAcceptanceTest or install Teamcal plugin.

## [3.1.6] - 2019-07-12

### Fixed

* [BSP-469]: AO library and plugin versions have become out of step.

## [3.0.3] - 2019-06-24

### Changed

* [CONFSRVDEV-11147]: ActiveObjects 3.0.2 throws an error on startup in Postgres.

## [3.0.2] - 2019-06-07

### Fixed

* [CONFSRVDEV-10940]: Fix confluence startup due to activeobjects-confluence-spi import of java.sql package.

## [3.0.1] - 2019-05-10

### Changed

* [AO-3459]: Update platform 5 POM to final.
* [AO-697]: Disable server side prepared statements for upgrade tasks when using postgres DBs.

### Fixed

* [AO-3480]: Possible thread leak in ao-plugin 3.0.0
* [AO-403]: Fix AO plugin cross-db build

## [3.0.0] - 2018-12-11

### Changed
* Includes AO library 3.0.0
* Set Java source and target version to 1.8
* Updated various maven plugins to versions that are compatible with Java 9, 10 and 11
* Upgraded to Mockito 2 for Java 11 compatibility
* Switched from commons-lang to commons-lang 3

## [2.1.0] - Unreleased

### Fixed

* [AO-3501]: Fixed DoS vulnerability through Woodstox dependency

## [2.0.0] - 2018-07-19

### Fixed

* [AO-3406]: Fixed unnecessary migrations for LONGTEXT datatype in MySQL DB

### Removed

* [AO-3406]: Removed automatic migration from TEXT to LONGTEXT in MySQL DB introduced in [AO-396].

### Changed

* Includes [AO library 2.0.0]

[AO library 2.0.0]: https://bitbucket.org/activeobjects/ao/src/master/CHANGELOG.md#markdown-header-200-2018-07-17
[AO-3406]: https://ecosystem.atlassian.net/browse/AO-3406
[AO-396]: https://ecosystem.atlassian.net/browse/AO-396

## [1.5.2] - 2018-06-13

### Fixed

* [AO-3466](https://ecosystem.atlassian.net/browse/AO-3466): `ActiveObjectsSqlException` no longer attempts to gather
  database and driver details when the `SQLException` being handled is an instance of any of the following:

  - `SQLNonTransientConnectionException`
  - `SQLTransientConnectionException`
  
  This is intended to reduce the likelihood that exceptions thrown by Active Objects exacerbate situations where the
  underlying database pool has been exhausted.

### Changed

* Includes [AO library 1.5.2]

[1.5.2]: https://bitbucket.org/activeobjects/ao-plugin/commits/tag/activeobjects-plugin-parent-pom-1.5.0
[AO-397]: https://ecosystem.atlassian.net/browse/AO-697
[AO-3466]: https://ecosystem.atlassian.net/browse/AO-3466
[AO library 1.5.2]: https://bitbucket.org/activeobjects/ao/src/master/CHANGELOG.md#markdown-header-152-2018-06-13
