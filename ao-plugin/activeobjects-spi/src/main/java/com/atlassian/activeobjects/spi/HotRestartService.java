package com.atlassian.activeobjects.spi;

import java.util.concurrent.Future;

/**
 * An interface which allows to explicitly restart AO.
 */
public interface HotRestartService {

    /**
     * Hot restart function which will indicate when AO is initialised successfully
     *
     * @return a Future that indicates when all plugins have restarted, value of the future is not relevant
     */
    Future<?> doHotRestart();
}
