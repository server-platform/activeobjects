package com.atlassian.activeobjects.spi;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadFactory;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.util.concurrent.ThreadFactoryBuilder;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.sal.api.executor.ThreadLocalDelegateExecutorFactory;

import static java.lang.Integer.getInteger;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.Executors.newFixedThreadPool;

public class DefaultInitExecutorServiceProvider implements InitExecutorServiceProvider {

    private static final Logger logger = LoggerFactory.getLogger(DefaultInitExecutorServiceProvider.class);

    private final ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory;

    @VisibleForTesting
    final ThreadFactory aoContextThreadFactory;

    public DefaultInitExecutorServiceProvider(
            final ThreadLocalDelegateExecutorFactory threadLocalDelegateExecutorFactory) {
        this.threadLocalDelegateExecutorFactory = requireNonNull(threadLocalDelegateExecutorFactory);

        // store the CCL of the ao-plugin bundle for use by all shared thread pool executors
        ClassLoader bundleContextClassLoader = Thread.currentThread().getContextClassLoader();
        aoContextThreadFactory = new ContextClassLoaderThreadFactory(bundleContextClassLoader);
    }

    /**
     * Create a thread pool executor with the same context class loader that was used when creating this class.
     *
     * Pool size is <code>activeobjects.servicefactory.ddl.threadpoolsize</code>, default 1.
     *
     * Runs in the same thread local context as the calling code.
     */
    @Nonnull
    @Override
    public ExecutorService initExecutorService() {
        logger.debug("creating default init executor");
        final ThreadFactory threadFactory = new ThreadFactoryBuilder()
                .setThreadFactory(aoContextThreadFactory)
                .setNameFormat("active-objects-init-%d")
                .build();
        final int threadPoolSize = getInteger("activeobjects.servicefactory.ddl.threadpoolsize", 1);
        final ExecutorService delegate = newFixedThreadPool(threadPoolSize, threadFactory);
        return threadLocalDelegateExecutorFactory.createExecutorService(delegate);
    }
}
