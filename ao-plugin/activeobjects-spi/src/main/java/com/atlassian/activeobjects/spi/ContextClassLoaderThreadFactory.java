package com.atlassian.activeobjects.spi;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import javax.annotation.Nonnull;

import com.atlassian.annotations.VisibleForTesting;

import static java.util.Objects.requireNonNull;

public class ContextClassLoaderThreadFactory implements ThreadFactory {

    private final ClassLoader contextClassLoader;

    public ContextClassLoaderThreadFactory(final ClassLoader contextClassLoader) {
        this.contextClassLoader = requireNonNull(contextClassLoader);
    }

    @Override
    public Thread newThread(@Nonnull final Runnable r) {
        final Thread thread = Executors.defaultThreadFactory().newThread(r);
        thread.setContextClassLoader(contextClassLoader);
        return thread;
    }

    @VisibleForTesting
    public ClassLoader getContextClassLoader() {
        return contextClassLoader;
    }
}
