package com.atlassian.activeobjects.spi;

import com.atlassian.annotations.PublicSpi;

/**
 * Configuration settings for the ActiveObjects plugin. Meant to be provided by the embedding product.
 */
@PublicSpi
public interface ActiveObjectsPluginConfiguration {

    /**
     * @return the base directory for plugin databases, resolved from the product's home directory
     */
    String getDatabaseBaseDirectory();
}
