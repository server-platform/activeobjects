package com.atlassian.activeobjects.external;

/**
 * This exception means something went wrong while attempting to run {@link ActiveObjects#getFastCountEstimate(Class)}
 *
 * @since 4.1
 */
public class FailedFastCountException extends Exception {

    public FailedFastCountException(Throwable cause) {
        super(cause);
    }
}
