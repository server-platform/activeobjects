package com.atlassian.activeobjects.backup;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import net.java.ao.test.jdbc.NonTransactional;

import com.atlassian.activeobjects.junit.HsqlTest;

import static com.atlassian.activeobjects.backup.ActiveObjectsBackupDataSetup.DatabaseBackup.HSQL;
import static com.atlassian.activeobjects.backup.ActiveObjectsBackupDataSetup.DatabaseBackup.HSQL_EMPTY;

@Category(HsqlTest.class)
public class TestHsqlActiveObjectBackup extends ActiveObjectsBackupDataSetup {

    @Test
    @NonTransactional
    public void testHsqlBackup() throws Exception {
        testBackup(HSQL);
    }

    @Test
    @NonTransactional
    public void testHsqlEmptyBackup() throws Exception {
        final String xmlBackup = read(HSQL_EMPTY.backupFile);

        /* Empty backup is a subset of the test backup data, therefore
           everything in the empty backup set is guaranteed to be found
           in the full backup data.
        */
        checkXmlBackup(xmlBackup, HSQL_DATA);
        restore(xmlBackup);
    }
}
