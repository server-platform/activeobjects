package com.atlassian.activeobjects.backup;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import net.java.ao.test.jdbc.NonTransactional;

import com.atlassian.activeobjects.junit.PostgresTest;

import static com.atlassian.activeobjects.backup.ActiveObjectsBackupDataSetup.DatabaseBackup.POSTGRES;

@Category(PostgresTest.class)
public class TestPostgresActiveObjectBackup extends ActiveObjectsBackupDataSetup {
    @Test
    @NonTransactional
    public void testPostgresBackup() throws Exception {
        testBackup(POSTGRES);
    }
}
