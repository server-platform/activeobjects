package com.atlassian.activeobjects.backup;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import net.java.ao.test.jdbc.NonTransactional;

import com.atlassian.activeobjects.junit.SqlServerTest;

import static com.atlassian.activeobjects.backup.ActiveObjectsBackupDataSetup.DatabaseBackup.SQLSERVER_JTDS;
import static com.atlassian.activeobjects.backup.ActiveObjectsBackupDataSetup.DatabaseBackup.SQLSERVER_MSJDBC;

@Category(SqlServerTest.class)
public class TestSqlServerActiveObjectBackup extends ActiveObjectsBackupDataSetup {
    @Test
    @NonTransactional
    public void testJtdcSqlServerBackup() throws Exception {
        testBackup(SQLSERVER_JTDS);
    }

    @Test
    @NonTransactional
    public void testMsjdbcSqlServerBackup() throws Exception {
        testBackup(SQLSERVER_MSJDBC);
    }
}
