package com.atlassian.activeobjects.backup;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import net.java.ao.test.jdbc.NonTransactional;

import com.atlassian.activeobjects.junit.MySqlTest;

import static com.atlassian.activeobjects.backup.ActiveObjectsBackupDataSetup.DatabaseBackup.MYSQL;

@Category(MySqlTest.class)
public class TestMySqlActiveObjectBackup extends ActiveObjectsBackupDataSetup {

    @Test
    @NonTransactional
    public void testMySqlBackup() throws Exception {
        testBackup(MYSQL);
    }
}
