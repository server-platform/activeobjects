package com.atlassian.activeobjects.backup;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import net.java.ao.test.jdbc.NonTransactional;

import com.atlassian.activeobjects.junit.H2Test;

import static com.atlassian.activeobjects.backup.ActiveObjectsBackupDataSetup.DatabaseBackup.H2;
import static com.atlassian.activeobjects.backup.ActiveObjectsBackupDataSetup.DatabaseBackup.H2_2_1;

@Category(H2Test.class)
public class TestH2ActiveObjectBackup extends ActiveObjectsBackupDataSetup {

    @Test
    @NonTransactional
    public void testH2Backup() throws Exception {
        testBackup(H2);
    }

    @Test
    @NonTransactional
    public void testH2_2_1_XBackup() throws Exception {
        testBackup(H2_2_1);
    }
}
