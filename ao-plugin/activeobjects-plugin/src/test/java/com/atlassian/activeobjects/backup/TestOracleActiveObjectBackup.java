package com.atlassian.activeobjects.backup;

import org.junit.Test;
import org.junit.experimental.categories.Category;

import net.java.ao.test.jdbc.NonTransactional;

import com.atlassian.activeobjects.junit.OracleTest;

import static com.atlassian.activeobjects.backup.ActiveObjectsBackupDataSetup.DatabaseBackup.ORACLE;
import static com.atlassian.activeobjects.backup.ActiveObjectsBackupDataSetup.DatabaseBackup.ORACLE_LEGACY;

@Category(OracleTest.class)
public class TestOracleActiveObjectBackup extends ActiveObjectsBackupDataSetup {

    @Test
    @NonTransactional
    public void testOracleBackup() throws Exception {
        testBackup(ORACLE);
    }

    @Test
    @NonTransactional
    public void testLegacyOracleBackup() throws Exception {
        testBackup(ORACLE_LEGACY);
    }
}
