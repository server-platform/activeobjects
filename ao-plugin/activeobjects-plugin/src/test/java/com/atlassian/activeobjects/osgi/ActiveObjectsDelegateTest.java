package com.atlassian.activeobjects.osgi;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.function.Supplier;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceEvent;
import org.osgi.framework.ServiceReference;
import com.google.common.util.concurrent.MoreExecutors;
import com.google.common.util.concurrent.SettableFuture;
import io.atlassian.util.concurrent.Promise;
import io.atlassian.util.concurrent.Promises;

import com.atlassian.activeobjects.config.ActiveObjectsConfiguration;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.internal.ActiveObjectsFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ActiveObjectsDelegateTest {
    private static final String SINGLETON = "singleton";

    private ActiveObjectsDelegate babyBear;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private Bundle bundle;

    @Mock
    private ActiveObjectsFactory factory;

    @Mock
    private Supplier<ExecutorService> initExecutorSupplier;

    @Mock
    private BundleContext bundleContext;

    @Mock
    private ServiceEvent serviceEvent;

    @Mock
    private ServiceReference serviceReference;

    @Mock
    private ActiveObjects ao;

    @Mock
    private ActiveObjectsConfiguration aoConfig1;

    @Mock
    private ActiveObjectsConfiguration aoConfig2;

    @Before
    public void before() {
        babyBear = new ActiveObjectsDelegate(bundle, factory, initExecutorSupplier);

        when(bundle.getSymbolicName()).thenReturn("some.bundle");
        when(bundle.getBundleContext()).thenReturn(bundleContext);

        when(bundleContext.getService(serviceReference)).thenReturn(aoConfig1);

        when(serviceEvent.getServiceReference()).thenReturn(serviceReference);
    }

    @Test
    public void init() {

        babyBear.init();

        assertThat(babyBear.aoPromisesBySingleton.asMap().keySet(), hasItem(SINGLETON));
    }

    @Test
    public void setAoConfig() throws ExecutionException, InterruptedException {
        babyBear.setAoConfiguration(aoConfig1);

        assertThat(babyBear.aoConfigFuture.isDone(), is(true));
        assertThat(babyBear.aoConfigFuture.get(), is(aoConfig1));
    }

    @Test
    public void setAoConfigMultipleConfigurationsThrowsIllegalStateException() {
        babyBear.aoConfigFuture.set(aoConfig1);

        expectedException.expect(IllegalStateException.class);

        babyBear.setAoConfiguration(aoConfig2);
    }

    @Test
    public void setAoConfigSameConfigurationIsOK() {
        babyBear.aoConfigFuture.set(aoConfig1);
        babyBear.setAoConfiguration(aoConfig1);
    }

    @Test
    public void delegate() throws ExecutionException, InterruptedException {
        babyBear.aoConfigFuture.set(aoConfig1);

        final Promise<ActiveObjects> aoPromise = Promises.promise(ao);

        babyBear.aoPromisesBySingleton.put(SINGLETON, aoPromise);

        assertSame(babyBear.delegate().get(), ao);
    }

    @Test
    public void delegateNoConfig() {
        expectedException.expect(IllegalStateException.class);

        babyBear.delegate();
    }

    @Test
    public void isInitializedNoTenant() {
        assertThat(babyBear.moduleMetaData().isInitialized(), is(false));
    }

    @Test
    public void isInitializedNotComplete() {
        assertThat(babyBear.moduleMetaData().isInitialized(), is(false));
    }

    @Test
    public void isInitializedException() {

        final SettableFuture<ActiveObjects> aoFuture = SettableFuture.create();
        aoFuture.setException(new IllegalStateException());
        final Promise<ActiveObjects> aoPromise = Promises.forFuture(aoFuture, MoreExecutors.newDirectExecutorService());
        babyBear.aoPromisesBySingleton.put(SINGLETON, aoPromise);

        assertThat(babyBear.moduleMetaData().isInitialized(), is(false));
    }

    @Test
    public void isInitializedComplete() {
        final Promise<ActiveObjects> aoPromise = Promises.promise(ao);
        babyBear.aoPromisesBySingleton.put(SINGLETON, aoPromise);

        assertThat(babyBear.moduleMetaData().isInitialized(), is(true));
    }

    @Test
    public void isDataSourcePresentAlwaysYes() {
        assertThat(babyBear.moduleMetaData().isDataSourcePresent(), is(true));
    }

    @Test
    public void isDataSourcePresentYes() {
        assertThat(babyBear.moduleMetaData().isDataSourcePresent(), is(true));
    }
}
