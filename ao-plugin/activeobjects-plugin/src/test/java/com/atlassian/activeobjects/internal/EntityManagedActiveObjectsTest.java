package com.atlassian.activeobjects.internal;

import java.beans.PropertyChangeListener;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;
import java.util.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import com.google.common.collect.Sets;

import net.java.ao.*;

import com.atlassian.activeobjects.external.FailedFastCountException;
import com.atlassian.activeobjects.spi.DatabaseType;
import com.atlassian.sal.api.transaction.TransactionCallback;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest({EntityManager.class, DatabaseMetaData.class, Connection.class, DatabaseProvider.class})
public class EntityManagedActiveObjectsTest {

    private EntityManagedActiveObjects activeObjects;

    @Mock
    private EntityManager entityManager;

    @Mock
    private TransactionManager transactionManager;

    @Mock
    private DatabaseProvider mockedDbProvider;

    @Mock
    private DBParam dbParam;

    @Mock
    private EntityStreamCallback<SampleEntity, Long> callback;

    @Before
    public void setUp() throws Exception {
        PowerMockito.whenNew(EntityManager.class).withAnyArguments().thenReturn(entityManager);
        activeObjects = new EntityManagedActiveObjects(entityManager, transactionManager, DatabaseType.HSQL);
        when(entityManager.getProvider()).thenReturn(mockedDbProvider);
    }

    @Test
    public void testExecuteInTransaction() throws Exception {
        final DatabaseProvider databaseProvider = mockProvider();
        when(entityManager.getProvider()).thenReturn(databaseProvider);

        @SuppressWarnings({"unchecked"})
        final TransactionCallback<Object> callback = mock(TransactionCallback.class);
        activeObjects.executeInTransaction(callback);

        verify(transactionManager).doInTransaction(callback);
    }

    @Test
    public void testMigrateSuccessful() throws SQLException {
        activeObjects.migrate(SampleEntity.class);
        verify(entityManager).setPolymorphicTypeMapper(any());
        verify(entityManager).migrate(SampleEntity.class);
    }

    @Test
    public void testMigrateFailure() throws SQLException {
        doThrow(new SQLException()).when(entityManager).migrate(SampleEntity.class);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.migrate(SampleEntity.class);
        });
    }

    @Test
    public void testMigratedDestructivelySuccessful() throws SQLException {
        activeObjects.migrateDestructively(SampleEntity.class);
        verify(entityManager).setPolymorphicTypeMapper(any());
        verify(entityManager).migrateDestructively(SampleEntity.class);
    }

    @Test
    public void testMigratedDestructivelyFailure() throws Exception {
        doThrow(new SQLException()).when(entityManager).migrateDestructively(SampleEntity.class);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.migrateDestructively(SampleEntity.class);
        });
    }

    @Test
    public void testFlushAll() throws SQLException {
        activeObjects.flushAll();
        verify(entityManager).flushAll();
    }

    @Test
    public void testFlush() {
        activeObjects.flush();
        verify(entityManager).flush();
    }

    @Test
    public void testGetEntityManagerSuccessful() throws SQLException {
        Long[] id = newArray();
        SampleEntity[] expectedResults = getExpectedResults();
        when(entityManager.get(SampleEntity.class, id)).thenReturn(expectedResults);
        SampleEntity[] results = activeObjects.get(SampleEntity.class, id);
        assertArrayEquals(results, expectedResults);
    }

    @Test
    public void testGetEntityManagerError() throws SQLException {
        Long[] id = newArray();
        doThrow(new SQLException()).when(entityManager).get(SampleEntity.class, id);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.get(SampleEntity.class, id);
        });
    }

    @Test
    public void testGetEntityManagerWithArray() throws SQLException {
        Long[] id = newArray();
        SampleEntity[] expectedResults = getExpectedResults();
        when(entityManager.get(SampleEntity.class, id[0])).thenReturn(expectedResults[0]);
        SampleEntity result = activeObjects.get(SampleEntity.class, id[0]);
        assertEquals(result, expectedResults[0]);
    }

    @Test
    public void testGetEntityManagerWithArrayError() throws SQLException {
        Long[] id = newArray();
        doThrow(new SQLException()).when(entityManager).get(SampleEntity.class, id[0]);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.get(SampleEntity.class, id[0]);
        });
    }

    @Test
    public void testCreateWithDbParam() throws SQLException {
        SampleEntity expectedResults = getData();
        when(activeObjects.create(SampleEntity.class, dbParam)).thenReturn(expectedResults);
        SampleEntity result = activeObjects.create(SampleEntity.class, dbParam);
        assertEquals(result, expectedResults);
    }

    @Test
    public void testCreateWithDbParamError() throws SQLException {
        doThrow(new SQLException()).when(entityManager).create(SampleEntity.class, dbParam);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.create(SampleEntity.class, dbParam);
            ;
        });
    }

    @Test
    public void testCreateWithMapParams() throws SQLException {
        SampleEntity expectedResults = getData();
        Map<String, Object> params = new HashMap<>();
        when(entityManager.create(SampleEntity.class, params)).thenReturn(expectedResults);
        SampleEntity result = activeObjects.create(SampleEntity.class, params);
        assertEquals(result, expectedResults);
    }

    @Test
    public void testCreateWithMapParamError() throws SQLException {
        Map<String, Object> params = new HashMap<>();
        params.put("name", "test");
        doThrow(new SQLException()).when(entityManager).create(SampleEntity.class, params);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.create(SampleEntity.class, params);
            ;
        });
    }

    @Test
    public void testCreateWithListParam() throws SQLException {
        List<Map<String, Object>> rows = new ArrayList<>();
        Map<String, Object> params = new HashMap<>();
        params.put("name", "test");
        rows.add(params);
        activeObjects.create(SampleEntity.class, rows);
        verify(entityManager).create(SampleEntity.class, rows);
    }

    @Test
    public void testCreateWithListParamError() throws SQLException {
        List<Map<String, Object>> rows = new ArrayList<>();
        Map<String, Object> params = new HashMap<>();
        params.put("name", "test");
        rows.add(params);
        doThrow(new SQLException()).when(entityManager).create(SampleEntity.class, rows);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.create(SampleEntity.class, rows);
        });
    }

    @Test
    public void testDelete() throws SQLException {
        activeObjects.create(SampleEntity.class, dbParam);
        activeObjects.delete();
        verify(entityManager).delete();
    }

    @Test
    public void testDeleteError() throws SQLException {
        doThrow(new SQLException()).when(entityManager).delete();
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.delete();
        });
    }

    @Test
    public void testDeleteWithSQL() throws SQLException {
        int expectedResult = 1;
        when(entityManager.deleteWithSQL(SampleEntity.class, "name = ?", "test"))
                .thenReturn(expectedResult);
        int result = activeObjects.deleteWithSQL(SampleEntity.class, "name = ?", "test");
        assertEquals(expectedResult, result);
    }

    @Test
    public void testDeleteWithSqlError() throws SQLException {
        doThrow(new SQLException()).when(entityManager).deleteWithSQL(SampleEntity.class, "name = ?", "test");
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.deleteWithSQL(SampleEntity.class, "name = ?", "test");
        });
    }

    @Test
    public void testFind() throws SQLException {
        SampleEntity[] expectedResults = getExpectedResults();
        when(entityManager.find(SampleEntity.class)).thenReturn(expectedResults);
        SampleEntity[] result = activeObjects.find(SampleEntity.class);
        assertArrayEquals(result, expectedResults);
    }

    @Test
    public void testFindError() throws SQLException {
        doThrow(new SQLException()).when(entityManager).find(SampleEntity.class);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.find(SampleEntity.class);
        });
    }

    @Test
    public void testFindWithCriteriaAndParams() throws SQLException {
        SampleEntity[] expectedResults = getExpectedResults();
        when(entityManager.find(SampleEntity.class, "name = ?", "test")).thenReturn(expectedResults);
        SampleEntity[] results = activeObjects.find(SampleEntity.class, "name = ?", "test");
        assertArrayEquals(results, expectedResults);
    }

    @Test
    public void testFindWithCriteriaAndParamsError() throws SQLException {
        doThrow(new SQLException()).when(entityManager).find(SampleEntity.class, "name = ?", "test");
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.find(SampleEntity.class, "name = ?", "test");
        });
    }

    @Test
    public void testFindWithQuery() throws SQLException {
        SampleEntity[] expectedResults = getExpectedResults();
        Query query = Query.select().where("name = ?", "test");
        when(entityManager.find(SampleEntity.class, query)).thenReturn(expectedResults);
        SampleEntity[] results = activeObjects.find(SampleEntity.class, query);
        assertArrayEquals(results, expectedResults);
    }

    @Test
    public void testFindWithQueryError() throws SQLException {
        Query query = Query.select().where("name = ?", "test");
        doThrow(new SQLException()).when(entityManager).find(SampleEntity.class, query);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.find(SampleEntity.class, query);
        });
    }

    @Test
    public void testFindWithQueryAndField() throws SQLException {
        SampleEntity[] expectedResults = getExpectedResults();
        Query query = Query.select().where("name = ?", "test");
        when(entityManager.find(SampleEntity.class, "id", query)).thenReturn(expectedResults);
        SampleEntity[] results = activeObjects.find(SampleEntity.class, "id", query);
        assertArrayEquals(results, expectedResults);
    }

    @Test
    public void testFindWithQueryAndFieldError() throws SQLException {
        Query query = Query.select().where("name = ?", "test");
        doThrow(new SQLException()).when(entityManager).find(SampleEntity.class, "id", query);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.find(SampleEntity.class, "id", query);
        });
    }

    @Test
    public void testFindWithSQL() throws SQLException {
        SampleEntity[] expectedResults = getExpectedResults();
        when(entityManager.findWithSQL(SampleEntity.class, "name = ?", "test")).thenReturn(expectedResults);
        SampleEntity[] result = activeObjects.findWithSQL(SampleEntity.class, "name = ?", "test");
        assertArrayEquals(result, expectedResults);
    }

    @Test
    public void testFindWithSQLError() throws SQLException {
        doThrow(new SQLException()).when(entityManager).findWithSQL(SampleEntity.class, "name = ?", "test");
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.findWithSQL(SampleEntity.class, "name = ?", "test");
        });
    }

    @Test
    public void testStream() throws SQLException {
        Query query = Query.select().where("name = ?", "test");
        activeObjects.stream(SampleEntity.class, query, callback);
        verify(entityManager).stream(SampleEntity.class, query, callback);
    }

    @Test
    public void testStreamError() throws SQLException {
        Query query = Query.select().where("name = ?", "test");
        doThrow(new SQLException()).when(entityManager).stream(SampleEntity.class, query, callback);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.stream(SampleEntity.class, query, callback);
        });
    }

    @Test
    public void testStreamWithCallback() throws SQLException {
        activeObjects.stream(SampleEntity.class, callback);
        verify(entityManager).stream(SampleEntity.class, callback);
    }

    @Test
    public void testStreamCallbackError() throws SQLException {
        doThrow(new SQLException()).when(entityManager).stream(SampleEntity.class, callback);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.stream(SampleEntity.class, callback);
        });
    }

    @Test
    public void testCount() throws SQLException {
        int expectedResult = 1;
        when(entityManager.count(SampleEntity.class)).thenReturn(expectedResult);
        int result = activeObjects.count(SampleEntity.class);
        assertEquals(result, expectedResult);
    }

    @Test
    public void testCountError() throws SQLException {
        doThrow(new SQLException()).when(entityManager).count(SampleEntity.class);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.count(SampleEntity.class);
        });
    }

    @Test
    public void testCountWithParameters() throws SQLException {
        int expectedResult = 1;
        when(entityManager.count(SampleEntity.class, "name = ?", "test")).thenReturn(expectedResult);
        int result = activeObjects.count(SampleEntity.class, "name = ?", "test");
        assertEquals(result, expectedResult);
    }

    @Test
    public void testCountWithParametersError() throws SQLException {
        doThrow(new SQLException()).when(entityManager).count(SampleEntity.class, "name = ?", "test");
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.count(SampleEntity.class, "name = ?", "test");
        });
    }

    @Test
    public void testCountWithQuery() throws SQLException {
        int expectedResult = 1;
        Query query = Query.select().where("name = ?", "test");
        when(entityManager.count(SampleEntity.class, query)).thenReturn(expectedResult);
        int result = activeObjects.count(SampleEntity.class, query);
        assertEquals(result, expectedResult);
    }

    @Test
    public void testCountWithQueryError() throws SQLException {
        Query query = Query.select().where("name = ?", "test");
        doThrow(new SQLException()).when(entityManager).count(SampleEntity.class, query);
        assertThrows(ActiveObjectsSqlException.class, () -> {
            activeObjects.count(SampleEntity.class, query);
        });
    }

    @Test
    public void testGetFastCountEstimate()
            throws SQLException, FailedFastCountException, net.java.ao.FailedFastCountException {
        int expectedResult = 1;
        when(entityManager.getFastCountEstimate(SampleEntity.class)).thenReturn(expectedResult);
        int result = activeObjects.getFastCountEstimate(SampleEntity.class);
        assertEquals(result, expectedResult);
    }

    @Test
    public void testGetFastCountError()
            throws SQLException, net.java.ao.FailedFastCountException, FailedFastCountException {
        doThrow(net.java.ao.FailedFastCountException.class)
                .when(entityManager)
                .getFastCountEstimate(SampleEntity.class);
        assertThrows(FailedFastCountException.class, () -> {
            activeObjects.getFastCountEstimate(SampleEntity.class);
        });
    }

    private static Long[] newArray() {
        return new Long[] {1L};
    }

    private DatabaseProvider mockProvider() throws Exception {
        final DisposableDataSource disposableDataSource = mock(DisposableDataSource.class);
        final Connection connection = mock(Connection.class);
        final DatabaseMetaData metaData = mock(DatabaseMetaData.class);

        when(disposableDataSource.getConnection()).thenReturn(connection);
        when(connection.getMetaData()).thenReturn(metaData);
        when(metaData.getIdentifierQuoteString()).thenReturn("");

        return new DatabaseProvider(disposableDataSource, null) {
            @Override
            protected Set<String> getReservedWords() {
                return Sets.newHashSet();
            }
        };
    }

    private SampleEntity[] getExpectedResults() {
        return new SampleEntity[] {getData()};
    }

    private SampleEntity getData() {
        SampleEntity entity = new SampleEntity();
        entity.setId(1l);
        entity.setName("test");
        return entity;
    }

    private static class SampleEntity implements RawEntity<Long> {
        Long id;
        String name;

        public Long getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public void init() {}

        @Override
        public void save() {}

        @Override
        public EntityManager getEntityManager() {
            return null;
        }

        @Override
        public <X extends RawEntity<Long>> Class<X> getEntityType() {
            return null;
        }

        @Override
        public void addPropertyChangeListener(PropertyChangeListener listener) {}

        @Override
        public void removePropertyChangeListener(PropertyChangeListener listener) {}
    }
}
