package com.atlassian.activeobjects.external;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.activeobjects.tx.Transactional;
import com.atlassian.sal.api.transaction.TransactionCallback;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

/**
 * Testing {@link com.atlassian.activeobjects.external.TransactionalAnnotationProcessor}
 */
@RunWith(MockitoJUnitRunner.class)
public class TransactionalAnnotationProcessorTest {
    private TransactionalAnnotationProcessor transactionalAnnotationProcessor;

    @Mock
    @SuppressWarnings("unused")
    private ActiveObjects ao;

    @Before
    public void setUp() {
        transactionalAnnotationProcessor = new TransactionalAnnotationProcessor(ao);
    }

    @After
    public void tearDown() {
        transactionalAnnotationProcessor = null;
    }

    @Test
    public void testPostProcessAfterInitializationDoesNothingWhenNotAnnotated() {
        final Object o = new Object();
        assertSame(o, transactionalAnnotationProcessor.postProcessAfterInitialization(o, "a-bean-name"));
    }

    @Test
    public void testPostProcessAfterInitializationReturnsProxyWhenAnnotatedAtClassLevel() {
        final Object o = new AnnotatedInterface() {};
        final Object proxy = transactionalAnnotationProcessor.postProcessAfterInitialization(o, "a-bean-name");
        assertNotSame(o, proxy);
    }

    @Test
    public void testPostProcessAfterInitializationReturnsProxyWhenAnnotatedAtMethodLevel() {
        final Object o = (AnnotatedMethodInInterface) () -> {};
        final Object proxy = transactionalAnnotationProcessor.postProcessAfterInitialization(o, "a-bean-name");
        assertNotSame(o, proxy);
    }

    @Test
    public void throwingExceptionInTransactionalMethodActuallyThrowsSameException() {
        when(ao.executeInTransaction(any()))
                .thenAnswer(invocation -> ((TransactionCallback) invocation.getArgument(0)).doInTransaction());

        final RuntimeException expectedException = new RuntimeException();
        final Object o = (AnnotatedMethodInInterface) () -> {
            throw expectedException;
        };

        final AnnotatedMethodInInterface proxy = (AnnotatedMethodInInterface)
                transactionalAnnotationProcessor.postProcessAfterInitialization(o, "a-bean-name");
        assertNotSame(o, proxy);
        try {
            proxy.doSomething();
        } catch (Exception actualException) {
            assertSame(expectedException, actualException);
        }
    }

    @Transactional
    public interface AnnotatedInterface {}

    public interface AnnotatedMethodInInterface {
        @Transactional
        @SuppressWarnings("unused")
        void doSomething();
    }
}
