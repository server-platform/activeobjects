package com.atlassian.activeobjects.plugin;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;

public class TestableUpgradeTaskClass implements ActiveObjectsUpgradeTask {
    @Override
    public ModelVersion getModelVersion() {
        return null;
    }

    @Override
    public void upgrade(ModelVersion currentVersion, ActiveObjects ao) {}
}
