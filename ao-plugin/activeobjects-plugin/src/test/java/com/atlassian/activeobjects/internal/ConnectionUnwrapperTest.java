package com.atlassian.activeobjects.internal;

import java.lang.reflect.Constructor;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.util.Optional;

import org.apache.commons.dbcp2.PoolingDataSource;
import org.apache.commons.pool2.ObjectPool;
import org.junit.Test;
import com.mchange.v2.c3p0.impl.NewProxyConnection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Check different cases of how connections are expected to be unwrapped.
 */
public class ConnectionUnwrapperTest {

    @Test
    public void unwrappingOfHikariCPConnections() throws Exception {
        final Connection inner = mock(Connection.class);
        final DatabaseMetaData outerMetadata = mock(DatabaseMetaData.class);
        final Connection outer = mock(Connection.class);

        when(outer.getMetaData()).thenReturn(outerMetadata);
        when(outerMetadata.getConnection()).thenReturn(inner);
        when(inner.unwrap(Connection.class)).thenReturn(inner);

        assertThat(ConnectionUnwrapper.tryUnwrapConnection(outer), equalTo(Optional.of(inner)));
    }

    @Test
    public void unwrappingOfPooledC3P0Connections() throws Exception {
        final Connection inner = mock(Connection.class);
        final Connection pooled = createPooledConnection(inner);
        final DatabaseMetaData outerMetadata = mock(DatabaseMetaData.class);
        final Connection outer = mock(Connection.class);

        when(outer.getMetaData()).thenReturn(outerMetadata);
        when(outerMetadata.getConnection()).thenReturn(pooled);
        when(pooled.unwrap(Connection.class)).thenReturn(pooled);

        assertThat(ConnectionUnwrapper.tryUnwrapConnection(outer), equalTo(Optional.of(inner)));
    }

    @Test
    public void unwrappingOfDBCP2Connection() throws Exception {
        final Connection inner = mock(Connection.class);
        final DatabaseMetaData outerMetadata = mock(DatabaseMetaData.class);
        final Connection outer = mock(Connection.class);

        when(outer.getMetaData()).thenReturn(outerMetadata);

        ObjectPool<Connection> objectPool = mock(ObjectPool.class);
        when(objectPool.borrowObject()).thenReturn(inner);
        try (PoolingDataSource<Connection> poolingDataSource = new PoolingDataSource<>(objectPool);
                Connection pooledConnection = poolingDataSource.getConnection()) {
            poolingDataSource.setAccessToUnderlyingConnectionAllowed(true);
            when(outerMetadata.getConnection()).thenReturn(pooledConnection);
            assertThat(ConnectionUnwrapper.tryUnwrapConnection(outer), equalTo(Optional.of(inner)));
        }
    }

    private Connection createPooledConnection(Connection inner) {
        try {
            final Constructor<NewProxyConnection> constructor =
                    NewProxyConnection.class.getDeclaredConstructor(Connection.class);
            constructor.setAccessible(true);
            return constructor.newInstance(inner);
        } catch (Exception e) {
            throw new RuntimeException("Wasn't able to create pooled connection. Please check what went wrong and "
                    + "fix the code to create an instance of NewPooledConnection with specific inner connection");
        }
    }
}
