package com.atlassian.activeobjects.internal;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import com.atlassian.util.profiling.MetricKey;
import com.atlassian.util.profiling.Metrics;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TimedActiveObjectsUpgradeTaskTest {

    @Rule
    public final MockitoRule rule = MockitoJUnit.rule();

    private static final ModelVersion MODEL_VERSION = ModelVersion.valueOf("4321");
    private static final String PLUGIN_KEY = "testingPluginKey";

    @Mock
    private ActiveObjectsUpgradeTask activeObjectsUpgradeTask;

    @Mock
    private MetricStrategy metricStrategy;

    @Mock
    private ActiveObjects activeObjects;

    private TimedActiveObjectsUpgradeTask timedActiveObjectsUpgradeTask;

    @BeforeClass
    public static void setupAll() {
        Metrics.getConfiguration().setEnabled(true);
    }

    @Before
    public void setup() {
        StrategiesRegistry.addMetricStrategy(metricStrategy);
        timedActiveObjectsUpgradeTask = new TimedActiveObjectsUpgradeTask(activeObjectsUpgradeTask, PLUGIN_KEY);
    }

    @Test
    public void testShouldCreateALongRunningTimer() {
        timedActiveObjectsUpgradeTask.upgrade(MODEL_VERSION, activeObjects);

        verify(metricStrategy).startLongRunningTimer(any(MetricKey.class));
    }

    @Test
    public void testShouldDelegateUpgrading() {
        timedActiveObjectsUpgradeTask.upgrade(MODEL_VERSION, activeObjects);

        verify(activeObjectsUpgradeTask).upgrade(MODEL_VERSION, activeObjects);
    }

    @Test
    public void testShouldDelegateGettingTheModelVersion() {
        when(activeObjectsUpgradeTask.getModelVersion()).thenReturn(MODEL_VERSION);
        final ModelVersion returnedModelVersion = timedActiveObjectsUpgradeTask.getModelVersion();

        verify(activeObjectsUpgradeTask).getModelVersion();
        assertEquals(MODEL_VERSION, returnedModelVersion);
    }

    @Test
    public void testShouldEndTimingAfterTaskFinished() {
        final Ticker ticker = mock(Ticker.class);
        when(metricStrategy.startLongRunningTimer(any(MetricKey.class))).thenReturn(ticker);

        verify(ticker, never()).close();
        timedActiveObjectsUpgradeTask.upgrade(MODEL_VERSION, activeObjects);
        verify(ticker).close();
    }

    @After
    public void teardown() {
        StrategiesRegistry.removeMetricStrategy(metricStrategy);
    }
}
