package com.atlassian.activeobjects.internal;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Testing {@link DataSourceTypeResolverImpl}
 */
@RunWith(MockitoJUnitRunner.class)
public class DataSourceTypeResolverImplTest {
    private static final Prefix PLUGIN_KEY = new SimplePrefix("bla");

    private DataSourceTypeResolver dataSourceTypeResolver;

    @Mock
    private PluginSettings pluginSettings;

    @Before
    public void setUp() {
        dataSourceTypeResolver = new DataSourceTypeResolverImpl(
                getMockPluginSettingsFactory(), new ActiveObjectsSettingKeys(), DataSourceType.APPLICATION);
    }

    @After
    public void tearDown() {
        dataSourceTypeResolver = null;
    }

    @Test
    public void testGetDataSourceTypeWithNoSettingReturnsDefaultValue() {
        when(pluginSettings.get(anyString())).thenReturn(null); // no setting at all
        assertEquals(DataSourceType.APPLICATION, dataSourceTypeResolver.getDataSourceType(PLUGIN_KEY));
    }

    @Test
    public void testGetDataSourceTypeWithNonDefaultSettingReturnsSetValue() {
        when(pluginSettings.get(anyString())).thenReturn(DataSourceType.HSQLDB.name());
        assertEquals(DataSourceType.HSQLDB, dataSourceTypeResolver.getDataSourceType(PLUGIN_KEY));
    }

    @Test
    public void testGetDataSourceTypeWithIncorrectSettingReturnsDefaultValue() {
        when(pluginSettings.get(anyString())).thenReturn("something-that-is-not-a-datasource-type");
        assertEquals(DataSourceType.APPLICATION, dataSourceTypeResolver.getDataSourceType(PLUGIN_KEY));
    }

    private PluginSettingsFactory getMockPluginSettingsFactory() {
        final PluginSettingsFactory pluginSettingFactory = mock(PluginSettingsFactory.class);
        when(pluginSettingFactory.createGlobalSettings()).thenReturn(pluginSettings);
        return pluginSettingFactory;
    }
}
