package com.atlassian.activeobjects.internal;

import java.util.concurrent.TimeUnit;
import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.Bundle;

import net.java.ao.EntityManager;

import com.atlassian.activeobjects.ActiveObjectsPluginException;
import com.atlassian.activeobjects.config.ActiveObjectsConfiguration;
import com.atlassian.activeobjects.config.PluginKey;
import com.atlassian.activeobjects.spi.DataSourceProvider;
import com.atlassian.activeobjects.spi.DatabaseType;
import com.atlassian.activeobjects.spi.TransactionSynchronisationManager;
import com.atlassian.beehive.ClusterLock;
import com.atlassian.beehive.ClusterLockService;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;

import static org.hamcrest.core.Is.isA;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Testing {@code DataSourceProviderActiveObjectsFactory}
 */
@RunWith(MockitoJUnitRunner.class)
public class DataSourceProviderActiveObjectsFactoryTest {
    private DataSourceProviderActiveObjectsFactory activeObjectsFactory;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private ActiveObjectUpgradeManager upgradeManager;

    @Mock
    private DataSourceProvider dataSourceProvider;

    @Mock
    private EntityManagerFactory entityManagerFactory;

    @Mock
    private TransactionTemplate transactionTemplate;

    @Mock
    private ActiveObjectsConfiguration configuration;

    @Mock
    private TransactionSynchronisationManager transactionSynchronizationManager;

    @Mock
    private ClusterLock clusterLock;

    @Before
    public void setUp() {
        Bundle bundle = mock(Bundle.class);
        when(bundle.getSymbolicName()).thenReturn("com.example.plugin");
        PluginKey pluginKey = PluginKey.fromBundle(bundle);
        ClusterLockService clusterLockService = mock(ClusterLockService.class);
        when(clusterLockService.getLockForName(anyString())).thenReturn(clusterLock);
        when(configuration.getDataSourceType()).thenReturn(DataSourceType.APPLICATION);
        when(configuration.getPluginKey()).thenReturn(pluginKey);
        activeObjectsFactory = new DataSourceProviderActiveObjectsFactory(
                upgradeManager, entityManagerFactory, dataSourceProvider, transactionTemplate, clusterLockService);
        activeObjectsFactory.setTransactionSynchronizationManager(transactionSynchronizationManager);
        when(transactionTemplate.execute(any()))
                .thenAnswer(invocation -> ((TransactionCallback<?>) invocation.getArgument(0)).doInTransaction());
    }

    @After
    public void tearDown() {
        activeObjectsFactory = null;
        entityManagerFactory = null;
        dataSourceProvider = null;
    }

    @Test
    public void testCreateWithNullDataSource() throws Exception {
        when(dataSourceProvider.getDataSource()).thenReturn(null); // not really needed, but just to make the test clear
        when(dataSourceProvider.getDatabaseType()).thenReturn(DatabaseType.DERBY_EMBEDDED);
        when(clusterLock.tryLock(AbstractActiveObjectsFactory.LOCK_TIMEOUT_SECONDS, TimeUnit.SECONDS))
                .thenReturn(true);
        try {
            activeObjectsFactory.create(configuration);
            fail("Should have thrown " + ActiveObjectsPluginException.class.getName());
        } catch (ActiveObjectsPluginException e) {
            verify(clusterLock).tryLock(AbstractActiveObjectsFactory.LOCK_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            verify(clusterLock).unlock();
        }
    }

    @Test
    public void testCreateWithNullDatabaseType() throws Exception {
        when(clusterLock.tryLock(AbstractActiveObjectsFactory.LOCK_TIMEOUT_SECONDS, TimeUnit.SECONDS))
                .thenReturn(true);
        when(dataSourceProvider.getDatabaseType())
                .thenReturn(null); // not really needed, but just to make the test clear
        try {
            activeObjectsFactory.create(configuration);
            fail("Should have thrown " + ActiveObjectsPluginException.class.getName());
        } catch (ActiveObjectsPluginException e) {
            verify(clusterLock).tryLock(AbstractActiveObjectsFactory.LOCK_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            verify(clusterLock).unlock();
        }
    }

    @Test
    public void testCreate() throws Exception {
        final DataSource dataSource = mock(DataSource.class);
        final EntityManager entityManager = mock(EntityManager.class);

        when(dataSourceProvider.getDataSource()).thenReturn(dataSource);
        when(entityManagerFactory.getEntityManager(anyDataSource(), anyDatabaseType(), isNull(), anyConfiguration()))
                .thenReturn(entityManager);
        when(configuration.getDataSourceType()).thenReturn(DataSourceType.APPLICATION);
        when(dataSourceProvider.getDatabaseType()).thenReturn(DatabaseType.DERBY_EMBEDDED);
        when(clusterLock.tryLock(AbstractActiveObjectsFactory.LOCK_TIMEOUT_SECONDS, TimeUnit.SECONDS))
                .thenReturn(true);

        assertNotNull(activeObjectsFactory.create(configuration));
        verify(entityManagerFactory).getEntityManager(anyDataSource(), anyDatabaseType(), isNull(), anyConfiguration());
        verify(clusterLock).tryLock(AbstractActiveObjectsFactory.LOCK_TIMEOUT_SECONDS, TimeUnit.SECONDS);
        verify(clusterLock).unlock();
    }

    @Test
    public void testCreateLockTimeout() throws InterruptedException {
        when(clusterLock.tryLock(AbstractActiveObjectsFactory.LOCK_TIMEOUT_SECONDS, TimeUnit.SECONDS))
                .thenReturn(false);

        expectedException.expect(ActiveObjectsInitException.class);
        expectedException.expectCause(nullValue(Throwable.class));

        activeObjectsFactory.create(configuration);
    }

    @Test
    public void testCreateLockInterrupted() throws InterruptedException {
        when(clusterLock.tryLock(AbstractActiveObjectsFactory.LOCK_TIMEOUT_SECONDS, TimeUnit.SECONDS))
                .thenThrow(new InterruptedException());

        expectedException.expect(ActiveObjectsInitException.class);
        expectedException.expectCause(isA(InterruptedException.class));

        activeObjectsFactory.create(configuration);
    }

    private static DataSource anyDataSource() {
        return Mockito.any();
    }

    private static DatabaseType anyDatabaseType() {
        return Mockito.any();
    }

    private static ActiveObjectsConfiguration anyConfiguration() {
        return Mockito.any();
    }
}
