package com.atlassian.activeobjects.plugin;

import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.internal.TimedActiveObjectsUpgradeTask;
import com.atlassian.plugin.module.Element;

import static java.util.Collections.singletonList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.Every.everyItem;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestActiveObjectModuleDescriptorTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void testUpgradeTasksWillBeTimed() {
        ActiveObjectModuleDescriptor underTest = mock(ActiveObjectModuleDescriptor.class);
        ActiveObjectsUpgradeTask upgradeTask = new TestableUpgradeTaskClass();

        Class<ActiveObjectsUpgradeTask> upgradeTaskClass = (Class<ActiveObjectsUpgradeTask>) upgradeTask.getClass();
        // Mock helper methods
        when(underTest.getUpgradeTaskElements(any())).thenReturn(singletonList(mock(Element.class)));
        when(underTest.getUpgradeTaskClasses(any())).thenReturn(singletonList(upgradeTaskClass));
        when(underTest.createBean(upgradeTaskClass)).thenReturn(upgradeTask);
        when(underTest.getPluginKey()).thenReturn("mock-plugin-key");
        // Expose the method under test
        when(underTest.getUpgradeTasks(any())).thenCallRealMethod();

        List<ActiveObjectsUpgradeTask> upgradeTasks = underTest.getUpgradeTasks(mock(Element.class));

        assertThat(upgradeTasks, is(not(empty())));
        assertThat(upgradeTasks, everyItem(instanceOf(TimedActiveObjectsUpgradeTask.class)));
    }
}
