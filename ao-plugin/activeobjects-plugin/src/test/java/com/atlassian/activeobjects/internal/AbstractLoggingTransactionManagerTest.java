package com.atlassian.activeobjects.internal;

import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.util.profiling.MetricKey;
import com.atlassian.util.profiling.MetricTag;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.strategy.MetricStrategy;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class AbstractLoggingTransactionManagerTest {

    private static final TransactionCallback<Object> CALLBACK = () -> null;

    @Mock
    private MetricStrategy metricStrategy;

    @Captor
    private ArgumentCaptor<MetricKey> metricCaptor;

    @Before
    public void setUp() {
        StrategiesRegistry.addMetricStrategy(metricStrategy);
    }

    @Test
    public void testStartLongRunningTimer() {
        DefaultTimedLoggingTransactionManager transactionManager = new DefaultTimedLoggingTransactionManager();

        transactionManager.doInTransaction(CALLBACK);

        verify(metricStrategy).startLongRunningTimer(metricCaptor.capture());
        assertThat(metricCaptor.getValue().getMetricName(), is("db.ao.executeInTransaction"));
        assertThat(
                "analytics should be captured",
                metricCaptor.getValue().getTags().stream()
                        .map(MetricTag::getKey)
                        .collect(Collectors.toList()),
                hasItem("atl-analytics"));
        assertTrue(
                "task name should be tagged",
                metricCaptor.getValue().getTags().stream()
                        .anyMatch(tag -> tag.getKey().equals("taskName")
                                && tag.getValue().contains(this.getClass().getName())));
    }

    public static class DefaultTimedLoggingTransactionManager extends AbstractLoggingTransactionManager {

        @Override
        <T> T inTransaction(TransactionCallback<T> callback) {
            return null;
        }
    }
}
