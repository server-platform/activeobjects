package com.atlassian.activeobjects.spring;

import org.springframework.beans.factory.FactoryBean;

import static java.util.Objects.requireNonNull;

/**
 * A {@link FactoryBean} that attempts to obtain the object as an OSGi service, but falls back to a given default
 * implementation (possibly another OSGi service) if that first service is not available.
 *
 * @param <T> the bean type
 */
public final class OptionalServiceFactoryBean<T> implements FactoryBean<T> {

    private final Class<T> type;
    private final T service;
    private final T defaultValue;

    public OptionalServiceFactoryBean(final Class<T> type, final T service, final T defaultValue) {
        this.type = requireNonNull(type);
        this.service = requireNonNull(service);
        this.defaultValue = requireNonNull(defaultValue);
    }

    @Override
    public T getObject() {
        try {
            //noinspection ResultOfMethodCallIgnored we just want to see if the OSGi service proxy is populated
            service.toString();
            return service;
        } catch (final RuntimeException e) {
            if ("ServiceUnavailableException".equals(e.getClass().getSimpleName())) {
                return defaultValue;
            }
            throw e;
        }
    }

    @Override
    public Class<T> getObjectType() {
        return type;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
