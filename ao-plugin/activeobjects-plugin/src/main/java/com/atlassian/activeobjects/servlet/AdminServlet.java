package com.atlassian.activeobjects.servlet;

import java.io.IOException;
import java.net.URI;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;

import com.atlassian.activeobjects.admin.tables.TablesController;
import com.atlassian.annotations.security.SystemAdminOnly;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.templaterenderer.TemplateRenderer;

@SystemAdminOnly
public class AdminServlet extends HttpServlet {

    private static final String TEMPLATE = "templates/list-tables.vm";

    private final AdminUi adminUi;
    private final TemplateRenderer templateRenderer;
    private final TablesController tablesController;
    private final UserManager userManager;
    private WebSudoManager webSudoManager;
    private final LoginUriProvider loginUriProvider;

    public AdminServlet(
            AdminUi adminUi,
            LoginUriProvider loginUriProvider,
            TablesController tablesController,
            TemplateRenderer templateRenderer,
            UserManager userManager,
            WebSudoManager webSudoManager) {
        this.adminUi = adminUi;
        this.loginUriProvider = loginUriProvider;
        this.tablesController = tablesController;
        this.templateRenderer = templateRenderer;
        this.userManager = userManager;
        this.webSudoManager = webSudoManager;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String pathInfo = req.getPathInfo();
        if (Strings.isNullOrEmpty(pathInfo) || !pathInfo.equals("/tables/list")) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        try {
            webSudoManager.willExecuteWebSudoRequest(req);

            UserProfile user = userManager.getRemoteUser(req);
            if (user == null) {
                sendRedirectToLogin(req, resp);
                return;
            }

            if (!isUserSysAdmin(user)) {
                resp.sendError(HttpServletResponse.SC_FORBIDDEN);
                return;
            }

            if (!adminUi.isEnabled()) {
                if (AdminUi.isDevModeEnabled()) {
                    resp.sendError(
                            HttpServletResponse.SC_NOT_FOUND,
                            "The Active Objects admin UI is disabled, see the logs for more information.");
                } else {
                    resp.sendError(HttpServletResponse.SC_NOT_FOUND);
                }
            }
            resp.setContentType("text/html;charset=UTF-8");
            templateRenderer.render(
                    TEMPLATE, ImmutableMap.of("tables", tablesController.listTables()), resp.getWriter());
        } catch (WebSudoSessionException ignored) {
            webSudoManager.enforceWebSudoProtection(req, resp);
        }
    }

    private boolean isUserSysAdmin(UserProfile user) {
        return user != null && userManager.isSystemAdmin(user.getUserKey());
    }

    private void sendRedirectToLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String requestUri = req.getRequestURI();
        String contextPath = req.getContextPath();
        if (!Strings.isNullOrEmpty(contextPath)) {
            requestUri = requestUri.substring(contextPath.length());
        }
        resp.sendRedirect(loginUriProvider.getLoginUri(URI.create(requestUri)).toString());
    }
}
