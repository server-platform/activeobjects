package com.atlassian.activeobjects.admin.tables;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.java.ao.DatabaseProvider;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;
import static net.java.ao.sql.SqlUtils.closeQuietly;

public final class RowCounter {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final DatabaseProvider provider;

    private RowCounter(final DatabaseProvider provider) {
        this.provider = requireNonNull(provider);
    }

    int count(final String tableName) {
        Connection connection = null;
        PreparedStatement stmt = null;
        ResultSet res = null;
        try {
            connection = provider.getConnection();
            stmt = provider.preparedStatement(connection, "SELECT COUNT(*) FROM " + provider.withSchema(tableName));
            res = stmt.executeQuery();

            checkState(res.next());
            return res.getInt(1);
        } catch (SQLException e) {
            logger.warn("Could not count number of rows for table '{}'", tableName);
            logger.warn("Here is the exception:", e);
            return -1;
        } finally {
            closeQuietly(res);
            closeQuietly(stmt);
            closeQuietly(connection);
        }
    }

    static RowCounter from(final DatabaseProvider provider) {
        return new RowCounter(provider);
    }
}
