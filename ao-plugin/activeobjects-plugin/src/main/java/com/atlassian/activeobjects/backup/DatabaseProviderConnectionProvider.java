package com.atlassian.activeobjects.backup;

import java.sql.Connection;
import java.sql.SQLException;

import net.java.ao.DatabaseProvider;

import com.atlassian.dbexporter.ConnectionProvider;

import static java.util.Objects.requireNonNull;

final class DatabaseProviderConnectionProvider implements ConnectionProvider {

    private final DatabaseProvider provider;

    public DatabaseProviderConnectionProvider(DatabaseProvider provider) {
        this.provider = requireNonNull(provider);
    }

    public Connection getConnection() throws SQLException {
        return provider.getConnection();
    }
}
