package com.atlassian.activeobjects.osgi;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.function.Supplier;
import javax.annotation.Nonnull;

import org.osgi.framework.Bundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import io.atlassian.util.concurrent.Promise;
import io.atlassian.util.concurrent.Promises;

import net.java.ao.DBParam;
import net.java.ao.EntityStreamCallback;
import net.java.ao.Query;
import net.java.ao.RawEntity;

import com.atlassian.activeobjects.config.ActiveObjectsConfiguration;
import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsModuleMetaData;
import com.atlassian.activeobjects.external.FailedFastCountException;
import com.atlassian.activeobjects.internal.ActiveObjectsFactory;
import com.atlassian.activeobjects.internal.ActiveObjectsInitException;
import com.atlassian.activeobjects.spi.DatabaseType;
import com.atlassian.sal.api.transaction.TransactionCallback;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Delegate for {@link com.atlassian.activeobjects.external.ActiveObjects}.
 * <p>
 * Baby bear is not to eager, not to lazy.
 * <p>
 * Delegate calls will block when DDL / updgrade tasks are running.
 * <p>
 * DDL / upgrade tasks will be initiated by the first call to the delegate or by a call to {@link #startActiveObjects}
 */
class ActiveObjectsDelegate implements ActiveObjects {
    private static final Logger logger = LoggerFactory.getLogger(ActiveObjectsDelegate.class);
    private static final String SINGLETON = "singleton";

    private final Bundle bundle;

    @VisibleForTesting
    final Promises.SettablePromise<ActiveObjectsConfiguration> aoConfigFuture = Promises.settablePromise();

    @VisibleForTesting
    final LoadingCache<String, Promise<ActiveObjects>> aoPromisesBySingleton;

    ActiveObjectsDelegate(
            @Nonnull final Bundle bundle,
            @Nonnull final ActiveObjectsFactory factory,
            @Nonnull final Supplier<ExecutorService> initExecutorSupplier) {
        this.bundle = checkNotNull(bundle);
        checkNotNull(factory);
        checkNotNull(initExecutorSupplier);

        // loading cache for delegate promises by singleton
        aoPromisesBySingleton = CacheBuilder.newBuilder().build(new CacheLoader<String, Promise<ActiveObjects>>() {
            @Override
            public Promise<ActiveObjects> load(@Nonnull final String singleton) {
                logger.debug("bundle [{}] loading new AO promise for {}", bundle.getSymbolicName(), singleton);

                return aoConfigFuture.flatMap(
                        (Function<ActiveObjectsConfiguration, Promise<ActiveObjects>>) aoConfig -> {
                            logger.debug("bundle [{}] got ActiveObjectsConfiguration ", bundle.getSymbolicName());

                            final Promises.SettablePromise<ActiveObjects> aoPromise = Promises.settablePromise();
                            //noinspection ConstantConditions
                            initExecutorSupplier.get().submit(() -> {
                                try {
                                    logger.debug("bundle [{}] creating ActiveObjects", bundle.getSymbolicName());
                                    final ActiveObjects ao = factory.create(aoConfig);
                                    logger.debug("bundle [{}] created ActiveObjects", bundle.getSymbolicName());
                                    aoPromise.set(ao);
                                } catch (Throwable t) {
                                    final ActiveObjectsInitException activeObjectsInitException =
                                            new ActiveObjectsInitException(
                                                    "bundle [" + bundle.getSymbolicName() + "]", t);
                                    aoPromise.exception(activeObjectsInitException);
                                    logger.warn(
                                            "bundle [{}] failed to create ActiveObjects", bundle.getSymbolicName(), t);
                                }
                                return null;
                            });

                            return aoPromise;
                        });
            }
        });
    }

    public void init() {
        logger.debug("init bundle [{}]", bundle.getSymbolicName());
        aoPromisesBySingleton.invalidate(SINGLETON);
        startActiveObjects();
    }

    public void destroy() {
        aoConfigFuture.cancel(false);
        for (Promise<ActiveObjects> aoPromise : aoPromisesBySingleton.asMap().values()) {
            aoPromise.cancel(false);
        }
    }

    void setAoConfiguration(@Nonnull final ActiveObjectsConfiguration aoConfiguration) {
        logger.debug("setAoConfiguration [{}]", bundle.getSymbolicName());

        if (aoConfigFuture.isDone()) {
            final ActiveObjectsConfiguration currentAoConfiguration;
            try {
                currentAoConfiguration = aoConfigFuture.get(0, TimeUnit.MILLISECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException e) {
                // we've already checked the state of the future, this exception must be dire
                throw new IllegalStateException(e);
            }

            if (currentAoConfiguration == aoConfiguration) {
                logger.debug("setAoConfiguration received same <ao> configuration twice [{}]", aoConfiguration);
            } else {
                final RuntimeException e = new IllegalStateException(
                        "bundle [" + bundle.getSymbolicName()
                                + "] has multiple active objects configurations - only one active objects module descriptor <ao> allowed per plugin!");
                aoConfigFuture.exception(e);
                throw e;
            }
        } else {
            aoConfigFuture.set(aoConfiguration);
        }
    }

    void startActiveObjects() {
        aoPromisesBySingleton.getUnchecked(SINGLETON);
    }

    Promise<ActiveObjects> restartActiveObjects() {
        aoPromisesBySingleton.invalidate(SINGLETON);
        return aoPromisesBySingleton.getUnchecked(SINGLETON);
    }

    @VisibleForTesting
    protected Promise<ActiveObjects> delegate() {
        if (!aoConfigFuture.isDone()) {
            throw new IllegalStateException(
                    "plugin [{" + bundle.getSymbolicName()
                            + "}] invoking ActiveObjects before <ao> configuration module is enabled or plugin is missing an <ao> configuration module. Note that scanning of entities from the ao.model package is no longer supported.");
        }

        return aoPromisesBySingleton.getUnchecked(SINGLETON);
    }

    @Override
    public ActiveObjectsModuleMetaData moduleMetaData() {
        return new ActiveObjectsModuleMetaData() {
            @Override
            public void awaitInitialization() throws ExecutionException, InterruptedException {
                aoPromisesBySingleton.getUnchecked(SINGLETON).get();
            }

            @Override
            public void awaitInitialization(long timeout, TimeUnit unit)
                    throws InterruptedException, ExecutionException, TimeoutException {
                aoPromisesBySingleton.getUnchecked(SINGLETON).get(timeout, unit);
            }

            @Override
            public boolean isInitialized() {
                Promise<ActiveObjects> aoPromise = aoPromisesBySingleton.getUnchecked(SINGLETON);
                if (aoPromise.isDone()) {
                    try {
                        aoPromise.claim();
                        return true;
                    } catch (Exception e) {
                        // any exception indicates a failure in initialisation, or at least that the delegate is not
                        // usable
                    }
                }
                return false;
            }

            @Override
            public DatabaseType getDatabaseType() {
                return delegate().claim().moduleMetaData().getDatabaseType();
            }

            @Override
            public boolean isDataSourcePresent() {
                return true;
            }

            @Override
            public boolean isTablePresent(Class<? extends RawEntity<?>> type) {
                return delegate().claim().moduleMetaData().isTablePresent(type);
            }
        };
    }

    @Override
    public void migrate(final Class<? extends RawEntity<?>>... entities) {
        delegate().claim().migrate(entities);
    }

    @Override
    public void migrateDestructively(final Class<? extends RawEntity<?>>... entities) {
        delegate().claim().migrateDestructively(entities);
    }

    @Override
    public void flushAll() {
        delegate().claim().flushAll();
    }

    @Override
    public void flush(final RawEntity<?>... entities) {
        delegate().claim().flush(entities);
    }

    @Override
    public <T extends RawEntity<K>, K> T[] get(final Class<T> type, final K... keys) {
        return delegate().claim().get(type, keys);
    }

    @Override
    public <T extends RawEntity<K>, K> T get(final Class<T> type, final K key) {
        return delegate().claim().get(type, key);
    }

    @Override
    public <T extends RawEntity<K>, K> T create(final Class<T> type, final DBParam... params) {
        return delegate().claim().create(type, params);
    }

    @Override
    public <T extends RawEntity<K>, K> T create(final Class<T> type, final Map<String, Object> params) {
        return delegate().claim().create(type, params);
    }

    @Override
    public <T extends RawEntity<K>, K> void create(Class<T> type, List<Map<String, Object>> rows) {
        delegate().claim().create(type, rows);
    }

    @Override
    public void delete(final RawEntity<?>... entities) {
        delegate().claim().delete(entities);
    }

    @Override
    public <K> int deleteWithSQL(
            final Class<? extends RawEntity<K>> type, final String criteria, final Object... parameters) {
        return delegate().claim().deleteWithSQL(type, criteria, parameters);
    }

    @Override
    public <T extends RawEntity<K>, K> T[] find(final Class<T> type) {
        return delegate().claim().find(type);
    }

    @Override
    public <T extends RawEntity<K>, K> T[] find(
            final Class<T> type, final String criteria, final Object... parameters) {
        return delegate().claim().find(type, criteria, parameters);
    }

    @Override
    public <T extends RawEntity<K>, K> T[] find(final Class<T> type, final Query query) {
        return delegate().claim().find(type, query);
    }

    @Override
    public <T extends RawEntity<K>, K> T[] find(final Class<T> type, final String field, final Query query) {
        return delegate().claim().find(type, field, query);
    }

    @Override
    public <T extends RawEntity<K>, K> T[] findWithSQL(
            final Class<T> type, final String keyField, final String sql, final Object... parameters) {
        return delegate().claim().findWithSQL(type, keyField, sql, parameters);
    }

    @Override
    public <T extends RawEntity<K>, K> void stream(
            final Class<T> type, final EntityStreamCallback<T, K> streamCallback) {
        delegate().claim().stream(type, streamCallback);
    }

    @Override
    public <T extends RawEntity<K>, K> void stream(
            final Class<T> type, final Query query, final EntityStreamCallback<T, K> streamCallback) {
        delegate().claim().stream(type, query, streamCallback);
    }

    @Override
    public <K> int count(final Class<? extends RawEntity<K>> type) {
        return delegate().claim().count(type);
    }

    @Override
    public <K> int count(final Class<? extends RawEntity<K>> type, final String criteria, final Object... parameters) {
        return delegate().claim().count(type, criteria, parameters);
    }

    @Override
    public <K> int count(final Class<? extends RawEntity<K>> type, final Query query) {
        return delegate().claim().count(type, query);
    }

    @Override
    public <K> int getFastCountEstimate(Class<? extends RawEntity<K>> type)
            throws SQLException, FailedFastCountException {
        return delegate().claim().getFastCountEstimate(type);
    }

    @Override
    public <T> T executeInTransaction(final TransactionCallback<T> callback) {
        return delegate().claim().executeInTransaction(callback);
    }

    public Bundle getBundle() {
        return bundle;
    }
}
