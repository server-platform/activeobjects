package com.atlassian.activeobjects.internal;

import javax.sql.DataSource;

import net.java.ao.EntityManager;

import com.atlassian.activeobjects.config.ActiveObjectsConfiguration;
import com.atlassian.activeobjects.spi.DatabaseType;

/**
 * A factory to create new EntityManagers from a given data source.
 */
public interface EntityManagerFactory {
    /**
     * Creates a <em>new</em> entity manager using the given data source.
     *
     * @param dataSource    the data source for which to create the entity manager
     * @param databaseType  the type of database that the data source connects to.
     * @param configuration the configuration for this active objects instance
     * @return a new entity manager
     */
    EntityManager getEntityManager(
            DataSource dataSource, DatabaseType databaseType, String schema, ActiveObjectsConfiguration configuration);
}
