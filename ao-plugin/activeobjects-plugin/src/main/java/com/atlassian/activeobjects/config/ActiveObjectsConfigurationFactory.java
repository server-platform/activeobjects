package com.atlassian.activeobjects.config;

import java.util.List;
import java.util.Set;

import org.osgi.framework.Bundle;

import net.java.ao.RawEntity;

import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;

public interface ActiveObjectsConfigurationFactory {
    ActiveObjectsConfiguration getConfiguration(
            Bundle bundle,
            String namespace,
            Set<Class<? extends RawEntity<?>>> entities,
            List<ActiveObjectsUpgradeTask> upgradeTasks);
}
