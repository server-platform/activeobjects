package com.atlassian.activeobjects;

import java.util.Set;

import net.java.ao.RawEntity;
import net.java.ao.schema.NameConverters;

public interface EntitiesValidator {
    Set<Class<? extends RawEntity<?>>> check(
            Set<Class<? extends RawEntity<?>>> entityClasses, NameConverters nameConverters);
}
