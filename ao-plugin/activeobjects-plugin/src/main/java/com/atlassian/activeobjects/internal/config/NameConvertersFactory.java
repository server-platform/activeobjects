package com.atlassian.activeobjects.internal.config;

import net.java.ao.schema.NameConverters;

import com.atlassian.activeobjects.internal.Prefix;

public interface NameConvertersFactory {
    NameConverters getNameConverters(Prefix prefix);
}
