package com.atlassian.activeobjects.admin.condition;

import java.util.Map;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

import static java.util.Objects.requireNonNull;

public final class UserIsSysAdminCondition implements Condition {

    private final UserManager userManager;

    public UserIsSysAdminCondition(final UserManager userManager) {
        this.userManager = requireNonNull(userManager);
    }

    public void init(final Map<String, String> params) throws PluginParseException {
        // Nothing to do
    }

    public boolean shouldDisplay(final Map<String, Object> context) {
        final UserKey userKey = userManager.getRemoteUserKey();
        return userKey != null && userManager.isSystemAdmin(userKey);
    }
}
