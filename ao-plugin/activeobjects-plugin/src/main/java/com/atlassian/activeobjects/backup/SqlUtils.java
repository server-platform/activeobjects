package com.atlassian.activeobjects.backup;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import javax.annotation.Nonnull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.java.ao.util.StringUtils;

import com.atlassian.dbexporter.Column;
import com.atlassian.dbexporter.ImportExportErrorService;
import com.atlassian.dbexporter.Table;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

import static com.atlassian.dbexporter.jdbc.JdbcUtils.closeQuietly;

/**
 * SQL-related utility methods.
 */
final class SqlUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger("net.java.ao.sql");

    private SqlUtils() {
        throw new UnsupportedOperationException("Not for instantiation");
    }

    static Iterable<TableColumnPair> tableColumnPairs(final Iterable<Table> tables) {
        return stream(tables.spliterator(), false)
                .flatMap(table -> getAutoIncrementColumns(table).stream())
                .collect(toList());
    }

    static void executeUpdate(ImportExportErrorService errorService, String tableName, Statement s, String sql) {
        try {
            if (!StringUtils.isBlank(sql)) {
                LOGGER.debug(sql);
                s.executeUpdate(sql);
            }
        } catch (SQLException e) {
            onSqlException(errorService, tableName, sql, e);
        }
    }

    private static void onSqlException(
            ImportExportErrorService errorService, String table, String sql, SQLException e) {
        if (sql.startsWith("DROP") && e.getMessage().contains("does not exist")) {
            LOGGER.debug("Ignoring exception for SQL <{}>", sql, e);
            return;
        }
        throw errorService.newImportExportSqlException(
                table, "Error executing update for SQL statement '" + sql + "'", e);
    }

    static void executeUpdate(
            ImportExportErrorService errorService, String tableName, Connection connection, String sql) {
        Statement stmt = null;
        try {
            stmt = connection.createStatement();
            executeUpdate(errorService, tableName, stmt, sql);
        } catch (SQLException e) {
            throw errorService.newImportExportSqlException(tableName, "", e);
        } finally {
            closeQuietly(stmt);
        }
    }

    static int getIntFromResultSet(ImportExportErrorService errorService, String tableName, ResultSet res) {
        try {
            return res.next() ? res.getInt(1) : 1;
        } catch (SQLException e) {
            throw errorService.newImportExportSqlException(tableName, "Error getting int value from result set.", e);
        }
    }

    static ResultSet executeQuery(ImportExportErrorService errorService, String tableName, Statement s, String sql) {
        try {
            return s.executeQuery(sql);
        } catch (SQLException e) {
            throw errorService.newImportExportSqlException(
                    tableName, "Error executing query for SQL statement '" + sql + "'", e);
        }
    }

    static class TableColumnPair {

        final Table table;
        final Column column;

        public TableColumnPair(final Table table, final Column column) {
            this.table = requireNonNull(table);
            this.column = requireNonNull(column);
        }
    }

    private static List<TableColumnPair> getAutoIncrementColumns(@Nonnull final Table table) {
        return table.getColumns().stream()
                .filter(Column::isAutoIncrement)
                .map(column -> new TableColumnPair(table, column))
                .collect(toList());
    }
}
