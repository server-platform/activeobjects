package com.atlassian.activeobjects.internal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.util.profiling.Ticker;

import static com.atlassian.util.profiling.Metrics.metric;

/**
 * An abstract implementation that log at debug level runtime exception that cross the boundary
 * of a transaction.
 */
abstract class AbstractLoggingTransactionManager implements TransactionManager {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static final String DB_AO_TRANSACTION_MANAGER_TIMER_NAME = "db.ao.executeInTransaction";
    private static final String TASK_NAME = "taskName";

    public final <T> T doInTransaction(TransactionCallback<T> callback) {
        try (Ticker ignored = metric(DB_AO_TRANSACTION_MANAGER_TIMER_NAME)
                .withAnalytics()
                .withInvokerPluginKey()
                .tag(TASK_NAME, callback.getClass().getName())
                .startLongRunningTimer()) {
            return inTransaction(callback);
        } catch (RuntimeException e) {
            logger.debug("Exception thrown within transaction", e);
            throw e;
        }
    }

    abstract <T> T inTransaction(TransactionCallback<T> callback);
}
