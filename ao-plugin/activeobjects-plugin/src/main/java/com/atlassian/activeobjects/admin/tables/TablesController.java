package com.atlassian.activeobjects.admin.tables;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import net.java.ao.DatabaseProvider;
import net.java.ao.schema.NameConverters;

import com.atlassian.activeobjects.backup.ActiveObjectsBackup;
import com.atlassian.activeobjects.backup.ActiveObjectsBackup.UpperCaseEntityNameProcessor;
import com.atlassian.activeobjects.backup.ActiveObjectsTableReader;
import com.atlassian.activeobjects.backup.PluginInformationFactory;
import com.atlassian.activeobjects.internal.DatabaseProviderFactory;
import com.atlassian.activeobjects.spi.DataSourceProvider;
import com.atlassian.activeobjects.spi.PluginInformation;
import com.atlassian.dbexporter.DatabaseInformation;
import com.atlassian.dbexporter.ImportExportErrorService;
import com.atlassian.dbexporter.Table;
import com.atlassian.dbexporter.exporter.TableReader;

import static java.util.Collections.emptyMap;
import static java.util.Objects.requireNonNull;

public final class TablesController {

    private final DatabaseProviderFactory databaseProviderFactory;
    private final ImportExportErrorService errorService;
    private final NameConverters nameConverters;
    private final PluginInformationFactory pluginInformationFactory;
    private final DataSourceProvider dataSourceProvider;

    public TablesController(
            final DatabaseProviderFactory databaseProviderFactory,
            final NameConverters nameConverters,
            final DataSourceProvider dataSourceProvider,
            final ImportExportErrorService errorService,
            final PluginInformationFactory pluginInformationFactory) {
        this.databaseProviderFactory = requireNonNull(databaseProviderFactory);
        this.errorService = requireNonNull(errorService);
        this.nameConverters = requireNonNull(nameConverters);
        this.pluginInformationFactory = requireNonNull(pluginInformationFactory);
        this.dataSourceProvider = requireNonNull(dataSourceProvider);
    }

    public Map<PluginInformation, List<TableInformation>> listTables() {
        final DatabaseProvider databaseProvider = getDatabaseProvider();
        final Iterable<Table> tables = readTables(newTableReader(databaseProvider));
        final RowCounter rowCounter = RowCounter.from(databaseProvider);
        return tablesPerPlugin(tables, rowCounter);
    }

    private Iterable<Table> readTables(final TableReader tableReader) {
        return tableReader.read(new DatabaseInformation(emptyMap()), new UpperCaseEntityNameProcessor());
    }

    private ActiveObjectsTableReader newTableReader(final DatabaseProvider databaseProvider) {
        return new ActiveObjectsTableReader(
                errorService, nameConverters, databaseProvider, ActiveObjectsBackup.schemaConfiguration());
    }

    private DatabaseProvider getDatabaseProvider() {
        return databaseProviderFactory.getDatabaseProvider(
                dataSourceProvider.getDataSource(),
                dataSourceProvider.getDatabaseType(),
                dataSourceProvider.getSchema());
    }

    private Map<PluginInformation, List<TableInformation>> tablesPerPlugin(
            final Iterable<Table> tables, final RowCounter rowCounter) {
        return StreamSupport.stream(tables.spliterator(), false)
                .collect(Collectors.groupingBy(
                        table -> newPluginInformation(table.getName()),
                        Collectors.mapping(
                                table -> newTableInformation(table.getName(), rowCounter), Collectors.toList())));
    }

    private PluginInformation newPluginInformation(final String tableName) {
        return pluginInformationFactory.getPluginInformation(tableName);
    }

    private TableInformation newTableInformation(final String tableName, final RowCounter rowCounter) {
        return new TableInformation(tableName, rowCounter.count(tableName));
    }

    public static final class TableInformation {

        private final String table;
        private final String rows;

        public TableInformation(final String table, final int rows) {
            this.table = requireNonNull(table);
            this.rows = String.valueOf(rows);
        }

        public String getTable() {
            return table;
        }

        public String getRows() {
            return rows;
        }
    }
}
