package com.atlassian.activeobjects.internal;

import javax.annotation.Nonnull;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.external.ActiveObjectsUpgradeTask;
import com.atlassian.activeobjects.external.ModelVersion;
import com.atlassian.util.profiling.Metrics;
import com.atlassian.util.profiling.Ticker;

import static java.util.Objects.requireNonNull;

/**
 * Times how long an upgrade task takes
 * @since 3.6.0
 */
public class TimedActiveObjectsUpgradeTask implements ActiveObjectsUpgradeTask {
    private static final String DB_AO_UPGRADE_TASK_TIMER_NAME = "db.ao.upgradeTask";
    private static final String TASK_NAME = "taskName";

    private final ActiveObjectsUpgradeTask activeObjectsUpgradeTask;
    private final String pluginKey;

    public TimedActiveObjectsUpgradeTask(
            @Nonnull final ActiveObjectsUpgradeTask activeObjectsUpgradeTask, @Nonnull final String pluginKey) {
        this.activeObjectsUpgradeTask = requireNonNull(activeObjectsUpgradeTask, "activeObjectsUpgradeTask");
        this.pluginKey = requireNonNull(pluginKey, "pluginKey");
    }

    @Override
    public ModelVersion getModelVersion() {
        return activeObjectsUpgradeTask.getModelVersion();
    }

    @Override
    public void upgrade(ModelVersion currentVersion, ActiveObjects ao) {
        try (Ticker ignored = Metrics.metric(DB_AO_UPGRADE_TASK_TIMER_NAME)
                .tag(TASK_NAME, activeObjectsUpgradeTask.getClass().getCanonicalName())
                .fromPluginKey(pluginKey)
                .withAnalytics()
                .startLongRunningTimer(); ) {
            activeObjectsUpgradeTask.upgrade(currentVersion, ao);
        }
    }
}
