package com.atlassian.activeobjects.admin;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginInformation;

import static java.util.Objects.requireNonNull;

public class PluginInfo {

    public final String key;
    public final String name;
    public final String version;
    public final String vendorName;
    public final String vendorUrl;

    public PluginInfo(
            final String key,
            final String name,
            final String version,
            final String vendorName,
            final String vendorUrl) {
        this.key = requireNonNull(key);
        this.name = requireNonNull(name);
        this.version = requireNonNull(version);
        this.vendorName = vendorName;
        this.vendorUrl = vendorUrl;
    }

    public static PluginInfo of(final Plugin plugin) {
        final PluginInformation pluginInformation = plugin.getPluginInformation();
        return new PluginInfo(
                plugin.getKey(),
                plugin.getName(),
                pluginInformation.getVersion(),
                pluginInformation.getVendorName(),
                pluginInformation.getVendorUrl());
    }
}
