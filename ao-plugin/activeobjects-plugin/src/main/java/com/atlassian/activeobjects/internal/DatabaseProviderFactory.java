package com.atlassian.activeobjects.internal;

import javax.annotation.Nonnull;
import javax.sql.DataSource;

import net.java.ao.DatabaseProvider;

import com.atlassian.activeobjects.spi.DatabaseType;

/**
 * A factory to create database provider given a data source
 */
public interface DatabaseProviderFactory {

    /**
     * Factory method for a database provider.
     *
     * @param dataSource the data source
     * @param databaseType the database type
     * @param schema the schema
     * @return a new instance
     */
    @Nonnull
    DatabaseProvider getDatabaseProvider(DataSource dataSource, DatabaseType databaseType, String schema);
}
