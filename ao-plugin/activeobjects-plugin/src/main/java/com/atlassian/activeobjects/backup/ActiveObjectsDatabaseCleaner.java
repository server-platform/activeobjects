package com.atlassian.activeobjects.backup;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Optional;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.java.ao.DatabaseProvider;
import net.java.ao.SchemaConfiguration;
import net.java.ao.schema.NameConverters;
import net.java.ao.schema.ddl.DDLAction;
import net.java.ao.schema.ddl.DDLTable;
import net.java.ao.schema.ddl.SQLAction;
import net.java.ao.schema.ddl.SchemaReader;

import com.atlassian.activeobjects.osgi.ActiveObjectsServiceFactory;
import com.atlassian.dbexporter.CleanupMode;
import com.atlassian.dbexporter.ImportExportErrorService;
import com.atlassian.dbexporter.importer.DatabaseCleaner;

import static java.util.Objects.requireNonNull;

import static com.atlassian.activeobjects.backup.SqlUtils.executeUpdate;
import static com.atlassian.dbexporter.jdbc.JdbcUtils.closeQuietly;

final class ActiveObjectsDatabaseCleaner implements DatabaseCleaner {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ActiveObjectsServiceFactory aoServiceFactory;
    private final DatabaseProvider provider;
    private final ImportExportErrorService errorService;
    private final NameConverters converters;
    private final SchemaConfiguration schemaConfiguration;

    public ActiveObjectsDatabaseCleaner(
            final DatabaseProvider provider,
            final NameConverters converters,
            final SchemaConfiguration schemaConfiguration,
            final ImportExportErrorService errorService,
            final ActiveObjectsServiceFactory aoServiceFactory) {
        this.aoServiceFactory = requireNonNull(aoServiceFactory);
        this.converters = requireNonNull(converters);
        this.errorService = requireNonNull(errorService);
        this.provider = requireNonNull(provider);
        this.schemaConfiguration = requireNonNull(schemaConfiguration);
    }

    @Override
    public void cleanup(CleanupMode cleanupMode) {
        if (cleanupMode.equals(CleanupMode.CLEAN)) {
            doCleanup();
        } else {
            logger.debug("Not cleaning up database before import. "
                    + "Any existing entity with the same name of entity being imported will make the import fail.");
        }
    }

    private void doCleanup() {
        Connection conn = null;
        Statement stmt = null;
        try {
            aoServiceFactory.startCleaning();

            final DDLTable[] readTables = SchemaReader.readSchema(provider, converters, schemaConfiguration);
            final DDLAction[] actions = SchemaReader.sortTopologically(SchemaReader.diffSchema(
                    provider.getTypeManager(), new DDLTable[0], readTables, provider.isCaseSensitive()));

            conn = provider.getConnection();
            stmt = conn.createStatement();

            for (DDLAction a : actions) {
                final Iterable<SQLAction> sqlActions = provider.renderAction(converters, a);
                for (SQLAction sql : sqlActions) {
                    executeUpdate(errorService, tableName(a), stmt, sql.getStatement());
                }
            }
        } catch (SQLException e) {
            throw errorService.newImportExportSqlException(null, "", e);
        } finally {
            closeQuietly(stmt);
            closeQuietly(conn);
            aoServiceFactory.stopCleaning();
        }
    }

    @Nullable
    private static String tableName(final DDLAction action) {
        return Optional.ofNullable(action)
                .map(DDLAction::getTable)
                .map(DDLTable::getName)
                .orElse(null);
    }
}
