package com.atlassian.activeobjects.internal;

import java.sql.Connection;
import java.sql.SQLException;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.sql.DataSource;

import net.java.ao.ActiveObjectsException;

@ParametersAreNonnullByDefault
public final class DriverNameExtractorImpl implements DriverNameExtractor {

    @Nonnull
    @Override
    public String getDriverName(final DataSource dataSource) {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
            return connection.getMetaData().getDriverName();
        } catch (SQLException e) {
            throw new ActiveObjectsException(e);
        } finally {
            closeQuietly(connection);
        }
    }

    private static void closeQuietly(@Nullable final Connection connection) {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
