package com.atlassian.activeobjects.osgi;

import java.util.List;
import java.util.concurrent.Future;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.spi.HotRestartService;

public class DelegatingHotRestartService implements HotRestartService {

    private final ActiveObjectsServiceFactory delegate;

    public DelegatingHotRestartService(ActiveObjectsServiceFactory delegate) {
        this.delegate = delegate;
    }

    @Override
    public Future<List<ActiveObjects>> doHotRestart() {
        return delegate.doHotRestart();
    }
}
