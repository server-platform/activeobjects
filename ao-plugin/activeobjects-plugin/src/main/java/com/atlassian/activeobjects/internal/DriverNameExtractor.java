package com.atlassian.activeobjects.internal;

import javax.annotation.Nonnull;
import javax.annotation.ParametersAreNonnullByDefault;
import javax.sql.DataSource;

/**
 * A class to resolve the driver class name from a given data source
 */
@ParametersAreNonnullByDefault
public interface DriverNameExtractor {

    /**
     * Gets the driver class name from the data source
     *
     * @param dataSource the data source to resolve the driver class name from
     * @return a driver class name
     */
    @Nonnull
    String getDriverName(DataSource dataSource);
}
