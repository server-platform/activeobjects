# Active Objects Plugin

## Description

Active Objects is an ORM (object relational mapping) layer in Atlassian products. This plugin integrates
[Active Objects](https://bitbucket.org/server-platform/ao/) into the products.

## Ownership

This project is owned by the Server Java Platform team (go/abracadabra).

## Atlassian Developer?

### Internal Documentation

[Development and maintenance documentation](https://ecosystem.atlassian.net/wiki/display/AO/Home)

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/AOPLUG)

## External User?

### Issues

Please raise any issues you find in [Jira](https://ecosystem.atlassian.net/browse/AO)

### Documentation

[Active Objects Documentation](https://developer.atlassian.com/display/DOCS/Active+Objects)

### Releasing

This plugin and the AO library should always be released in lockstep, even if only one of them has actual changes. This
is what the product teams expect. So if you need to change:
 * the library:
 
   * release a new version of it,
   * bump this plugin to depend on that version of the library (see `ao.version`),
   * bump this plugin's own version to be that same version, and
   * release this plugin.
   
 * just the plugin:
 
   * release a new version of the library (whether it's changed or not),
   * bump this plugin to depend on that new version of the library (see `ao.version`),
   * bump this plugin's own version to be that same version, and
   * release this plugin.
