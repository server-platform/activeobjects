# Active Objects

## Description

This repository contains 2 projects:

* [AO library](./ao) - an ORM (object relational mapping) layer in Atlassian products
* [AO plugin](./ao-plugin) - integrates AO library into the products

## Ownership

This project is owned by the Server Java Platform team (go/abracadabra).

## Atlassian Developer?

### Running

To start the AO plugin in Refapp:

1. from the project root: `mvn clean install -DskipTests`
2. `cd ao-plugin/activeobjects-plugin`
3. `mvn amps:run` or `mvn amps:debug`
4. QR is enabled, so you can redeploy the plugin from another terminal using `mvn package` (don't `clean`!)
5. you can list the AO tables [here](http://localhost:5990/refapp/plugins/servlet/active-objects/tables/list)

### Testing
1. Local testing: 'mvn clean verify' to run whole suite of unit and integration tests. 
2. Single unit test can be run from Intellij ide
3. Single integration test run: go to working directory of integration tests and run command: 'mvn -Dit.test="<nameOfTestedClass>#<nameOfTestedMethod>" verify'

### Internal Documentation

[Development and maintenance documentation](https://ecosystem.atlassian.net/wiki/display/AO/Home)

### Committing Guidelines

Please see [The Platform Rules of Engagement (go/proe)](http://go.atlassian.com/proe) for committing to this module.

### Builds

The Bamboo builds for this project are on [EcoBAC](https://ecosystem-bamboo.internal.atlassian.com/browse/AO)

### Releasing

Release process is executed as Bamboo stage. AO Library however is still published through external repository
and require additional, manual step to make it publicly available. Detailed information in [Releasing AO](https://hello.atlassian.net/wiki/spaces/AB/pages/435853459/Active+Objects)

### Updating database test matrix

In order to update database test matrix (db version or db driver), please update our [Bamboo Specs](https://bitbucket.org/atlassian/build-specs/src/master/). 
Full configuration can be found in the class `com.atlassian.plan.platform.ao.DatabaseTestPlan`. 
New driver should be added to `com.atlassian.plan.platform.ao.DatabaseConfig.DriverVersion`.
New database version should be configured by `com.atlassian.plan.platform.ao.DatabaseConfig.DbVersion`.

## External User?

### Issues

Please raise any issues you find in [Jira](https://ecosystem.atlassian.net/browse/AO)

### Documentation

[Active Objects Documentation](https://developer.atlassian.com/display/DOCS/Active+Objects)
