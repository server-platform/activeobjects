# Upgrade guide to 6.0

## Guava function type removals

### Constants

The following constants in `net.java.ao.schema.info.FieldInfo` will change from the Guava function types to their respective Java native function types.

* `IS_REQUIRED`
* `HAS_GENERATOR`
* `PLUCK_NAME`

Since Guava function types extend the Java native ones, it is safe to treat these as Java native functions.

### Method arguments

In the following methods, we will be replacing the Guava function arguments with Java native ones. In some cases, you can continue passing in the Guava functions, as they extend the Java native types. In some cases we switched to more specific types; you can use lambda notation, and it will automatically compile to the right function type.

| Type                                                              | Method                 | Argument              | Old type                           | New type                           |
| ----------------------------------------------------------------- | ---------------------- | --------------------- | ---------------------------------- | ---------------------------------- |
| `com.atlassian.activeobjects.internal.ActiveObjectUpgradeManager` | `upgrade`              | `ao`                  | `com.google.common.base.Supplier`  | `java.util.function.Supplier`      |
| `net.java.ao.DatabaseProvider`                                    | `renderFields`         | `filter`              | `com.google.common.base.Predicate` | `java.util.function.Predicate`     |
|                                                                   |                        | `render`              | `com.google.common.base.Function`  | `java.util.function.Function`      |
| `net.java.ao.sql.SqlUtils`                                        | `processWhereClause`   | `processor`           | `com.google.common.base.Function`  | `java.util.function.UnaryOperator` |
|                                                                   | `processOnClause`      | `processor`           | `com.google.common.base.Function`  | `java.util.function.UnaryOperator` |
|                                                                   | `processGroupByClause` | `columnNameProcessor` | `com.google.common.base.Function`  | `java.util.function.UnaryOperator` |
|                                                                   |                        | `tableNameProcessor`  | `com.google.common.base.Function`  | `java.util.function.UnaryOperator` |
|                                                                   | `processHavingClause`  | `columnNameProcessor` | `com.google.common.base.Function`  | `java.util.function.UnaryOperator` |
|                                                                   |                        | `tableNameProcessor`  | `com.google.common.base.Function`  | `java.util.function.UnaryOperator` |

## Guava collection type removals

The return types of the following methods have been replaced with Java interfaces. To allow for these methods to exist side by side during the deprecation method, these methods have been renamed as well.

| Type                                                        | Old method         | Old return type                                 | New method            | New return type                                  |
| ----------------------------------------------------------- | ------------------ | ----------------------------------------------- | --------------------- | ------------------------------------------------ |
| `com.atlassian.activeobjects.admin.tables.TablesController` | `list`             | `Multimap<PluginInformation, TableInformation>` | `listTables`          | `Map<PluginInformation, List<TableInformation>>` |
| `net.java.ao.types.LogicalType`                             | `getTypes`         | `ImmutableSet<Class<?>>`                        | `getAllTypes`         | `Set<Class<?>>`                                  |
|                                                             | `getJdbcReadTypes` | `ImmutableSet<Integer>`                         | `getAllJdbcReadTypes` | `Set<Integer>`                                   |
