# Changelog
All notable changes to the Active Objects will be documented in this file.

## [4.1.1] - 2022-10-05
- [QUICK-2882] Fix FailedFastCountException not being exposed externally

## [4.1.0] - 2022-09-26
### Added
- [QUICK-2882] Added new method to ActiveObjects, getFastCountEstimate which allows for fast counting of AO entities at the cost of accuracy

##[3.6.2]
### Fixed
- [ITASM3-70] Fixed forward compatibility with atlassian-profiling 4.0.0 by removing direct usages of `MetricTag`

[3.6.2]: https://bitbucket.org/server-platform/activeobjects/branches/compare/v3.6.2%0Dv3.6.0

[QUICK-2882]: https://bulldog.internal.atlassian.com/browse/QUICK-2882
[ITASM3-70]: https://bulldog.internal.atlassian.com/browse/ITASM3-70

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).
